//
//  Extension+URLComponents.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 27/03/2024.
//

import Foundation

// refer: https://medium.com/swift2go/building-safe-url-in-swift-using-urlcomponents-and-urlqueryitem-alfian-losari-510a7b1f3c7e

extension URLComponents {
    mutating func setQueryItems(with parameters: [String: Any]) {
        var newURLQueryItems: [URLQueryItem] = []
        parameters.forEach { (key: String, values: Any) in
            // refer: https://stackoverflow.com/a/25724652
            switch values {
            case let n as [String]:
                newURLQueryItems.append(contentsOf: n.sorted().map { item in
                    URLQueryItem(name: key, value: item)
                })
                
                break
                
            case let t as String:
                newURLQueryItems.append(URLQueryItem(name: key, value: t))
                
                break
                
            default:
                break
            }
        }
        
        queryItems = newURLQueryItems.sorted(by: { item1, item2 in
            return item1.name.caseInsensitiveCompare(item2.name) == .orderedAscending
        })
    }
}
