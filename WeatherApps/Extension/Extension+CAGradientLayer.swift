//
//  CAGradientLayer.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 05/04/2024.
//

import UIKit

// refer: https://www.youtube.com/watch?v=IybqIc175a8
// top to bottom as (0.5, 0), (0.5, 1)
// left to right as (0, 0.5), (1, 0.5)
// corner to corner as (0, 0), (1, 1)
//        let gra = CAGradientLayer()
//        gra.frame = view.bounds
//        gra.type = .radial
//        gra.colors = [
//            ColorSets.food_recipe_unselected.color()!.cgColor,
//            ColorSets.food_recipe_button.color()!.cgColor,
//            ColorSets.food_recipe_unselected.color()!.cgColor
//        ]
//        gra.locations = [0.0, 0.8, 1.0]
//        gra.startPoint = CGPoint(x: 0.5, y: 0.6)
//        gra.endPoint = CGPoint(x: 1.0, y: 1.0)
//
//        view.layer.addSublayer(gra)


// refer: https://developer.apple.com/tutorials/app-dev-training/creating-a-gradient-background

extension CAGradientLayer {
    static func gradientLayer(for style: ListStyle, in frame: CGRect) -> Self {
        let layer = Self()
        layer.colors = colors(for: style)
        layer.frame = frame
        return layer
    }
    
    private static func colors(for style: ListStyle) -> [CGColor] {
        let beginColor: ColorSets
        let endColor: ColorSets

        switch style {
        case .weather_style:
            beginColor = .gradient_begin
            endColor = .gradient_end
            
        case .food_recipe_style:
            beginColor = .food_recipe_gradient_begin
            endColor = .food_recipe_gradient_end
            
        case .movie_style:
            beginColor = .movie_background
            endColor = .movie_background
        }
        
        return [beginColor.color()!.cgColor, endColor.color()!.cgColor]
    }
}
