//
//  Extension+UIColor.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 29/02/2024.
//

import UIKit

extension UIColor {
    static func appColor(name: ColorSets) -> UIColor? {
        return UIColor(named: name.rawValue)
    }
}
