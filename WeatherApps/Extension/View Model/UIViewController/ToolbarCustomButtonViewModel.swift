//
//  ToolbarCustomButtonViewModel.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 15/04/2024.
//

import UIKit

/// For ExtensionUIViewController addPreviousNextableDoneButtonOnKeyboard function parameter use.
/// - Parameters:
///   - objects:Array of text fields or text view you want to set
///   - customButtonTitle:Setting customButton title. Default is nil.
///   - doneButtonTitle:Setting doneButton title. Default is nil.
///   - customButtonClosure:Closure for customButton clicked action. Default is nil.
///   - doneButtonClosure:Closure for doneButton clicked action. Default is nil.
///
///How to use
/// =============================================
///     //ToolbarCustomButtonViewModel initail set
///     ToolbarCustomButtonViewModel(obj: {text fields or text view},)
///     ToolbarCustomButtonViewModel(
///         obj: {text fields or text view},
///         customButtonTitle: {Title Text},
///         doneButtonTitle: {Title Text},
///         customButtonClosure: { _ in
///             // Action for customButton clicked
///         },
///         doneButtonClosure: { _ in
///             // Action for doneButton clicked
///         }
///     )
///
public struct ToolbarCustomButtonViewModel {
    public var obj: AnyObject?
    public var customButtonTitle: String?
    public var doneButtonTitle: String?
    public var customButtonClosure: ((UIBarButtonItem) -> Void)?
    public var doneButtonClosure: ((UIBarButtonItem) -> Void)?
    
    public init(obj: AnyObject, customButtonTitle: String? = nil, doneButtonTitle: String? = nil, customButtonClosure: ((UIBarButtonItem) -> Void)? = nil, doneButtonClosure: ((UIBarButtonItem) -> Void)? = nil) {
        self.obj = obj
        self.customButtonTitle = customButtonTitle
        self.doneButtonTitle = doneButtonTitle
        self.customButtonClosure = customButtonClosure
        self.doneButtonClosure = doneButtonClosure
    }
}

