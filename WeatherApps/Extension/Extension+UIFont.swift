//
//  Extension+UIFont.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 28/02/2024.
//

import UIKit

extension UIFont {
    static func montserratMedium(size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Medium", size: size) ?? .systemFont(ofSize: size)
    }
    
    static func montserratBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Bold", size: size) ?? .systemFont(ofSize: size)
    }
    
    static func montserratRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Regular", size: size) ?? .systemFont(ofSize: size)
    }
    
    static func latoMedium(size: CGFloat) -> UIFont {
        return UIFont(name: "Lato-Medium", size: size) ?? .systemFont(ofSize: size)
    }
    
    static func latoSemiBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Lato-Semibold", size: size) ?? .systemFont(ofSize: size)
    }
    
    static func latoRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "Lato-Regular", size: size) ?? .systemFont(ofSize: size)
    }
}
