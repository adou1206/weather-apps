//
//  Extension+Int.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 05/04/2024.
//

import Foundation

extension Double {
    // refer: https://stackoverflow.com/a/69645125
    func convertMinutesToHrMinuteString() -> String {
        let seconds = self * 60
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute]
        // refer: https://developer.apple.com/documentation/foundation/datecomponentsformatter/unitsstyle
        formatter.unitsStyle = .abbreviated
        
        let formattedString = formatter.string(from: TimeInterval(seconds))!
        
        return formattedString
    }
    
    // refer: https://stackoverflow.com/a/68839646
    func round(to decimalPlaces: Int) -> Double {
        let precisionNumber = pow(10,Double(decimalPlaces))
        var n = self // self is a current value of the Double that you will round
        n = n * precisionNumber
        n.round()
        n = n / precisionNumber
        return n
    }
}
