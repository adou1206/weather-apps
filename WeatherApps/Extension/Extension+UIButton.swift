//
//  Extension+UIButton.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 22/03/2024.
//

import UIKit

extension UIButton {
    func addShadowLayer(
        shadowColor: UIColor = UIColor.darkGray,
        shadowOffset: CGPoint = CGPoint(x: 1, y: 1),
        shadowOpacity: CGFloat = 0.5,
        shadowBlurRadius: CGFloat = 3.0
    ) {
        let shadowLayer = CAShapeLayer()

        //this button type does not use button configuration - check the main layer's corner radius
        shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: self.layer.cornerRadius).cgPath
        
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.fillColor = backgroundColor?.cgColor
        shadowLayer.shadowColor = shadowColor.cgColor
        shadowLayer.shadowOffset = CGSize(width: shadowOffset.x, height: shadowOffset.y)
        shadowLayer.shadowOpacity = Float(shadowOpacity)
        shadowLayer.shadowRadius = shadowBlurRadius
        layer.insertSublayer(shadowLayer, at: 0)
    }
}
