//
//  Extension+UIViewController.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 18/06/2022.
//

import UIKit

fileprivate var spinnerView: UIView?

extension UIViewController {
    /// add indicator spinner to cover entire page
    func showSpinner() {
        if spinnerView == nil {
            spinnerView = UIView(frame: view.bounds)
            spinnerView?.backgroundColor = .clear
            
            let indicator = UIActivityIndicatorView(style: .medium)
            indicator.color = (UITraitCollection.current.userInterfaceStyle == .dark ? .white : .black)
            indicator.center = spinnerView!.center
            indicator.startAnimating()
            spinnerView?.addSubview(indicator)
            view.addSubview(spinnerView!)
        }
    }
    
    /// remove indicator spinner from covered entire page
    func removeSpinner() {
        guard let spinner = spinnerView else { return }
        spinner.removeFromSuperview()
        spinnerView = nil
    }
    
    func alertWithTitle(
        _ title: String,
        message: String,
        button_title: String = "OK",
        accessibility_identifier: String = "",
        closure: (() -> Void)? = nil
    ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.view.accessibilityIdentifier = accessibility_identifier
        
        alert.addAction(UIAlertAction(title: button_title, style: .cancel, handler: { _ in
            if let closure = closure {
                closure()
            }
        }))
        
        showDetailViewController(alert, sender: self)
    }
    
    ///The process of setting the front / back button and finish button on the keyboard toolbar when the target text field or text view is active.
    /// - Parameters:
    ///   - objects:Array of ToolbarCustomButtonViewModel
    ///   - previousNextable:Whether to enable the front and back buttons
    ///
    ///How to use
    /// =============================================
    ///     //Text field keyboard toolbar settings
    ///     addPreviousNextableDoneButtonOnKeyboard(objects: [ToolbarCustomButtonViewModel(obj: {text fields or text view})], previousNextable: false)
    ///     addPreviousNextableDoneButtonOnKeyboard(objects: [
    ///     ToolbarCustomButtonViewModel(obj: {text fields or text view}),
    ///     ToolbarCustomButtonViewModel(
    ///         obj: {text fields or text view},
    ///         customButtonTitle: {Title Text},
    ///         doneButtonTitle: {Title Text},
    ///         customButtonClosure: { _ in
    ///             // Action for customButton clicked
    ///         },
    ///         doneButtonClosure: { _ in
    ///             // Action for doneButton clicked
    ///         }
    ///     )
    ///     ], previousNextable: true)
    ///
    // refer: https://linuxtut.com/en/d24056544c9f29875d91/
    func addPreviousNextableDoneButtonOnKeyboard(objects: [ToolbarCustomButtonViewModel], previousNextable: Bool = false) {
        for (index, object) in objects.enumerated() {
            var textField: UITextField?
            var textView: UITextView?
            let imageUp: UIImage? = UIImage(systemName: "chevron.up") ?? nil
            let imageDown: UIImage? = UIImage(systemName: "chevron.down") ?? nil
            
            if let tView = object.obj as? UITextView {
                textView = tView
            }else if let tField = object.obj as? UITextField {
                textField = tField
            }
            
            //Loop processing is performed for each text field.
            let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
            toolBar.barStyle = .default
            toolBar.accessibilityIdentifier = "keyboard_toolbar"
            ///Bar button item
            var items = [UIBarButtonItem]()
            
            // MARK:Front and back button settings
            
            if previousNextable {
                //When the front and back buttons are enabled
                let previousButton = UIBarButtonItem(image: imageUp, style: .plain, target: self, action: nil)
                previousButton.accessibilityIdentifier = "toolbar_go_up"
                
                if let firstTView = objects.first?.obj as? UITextView {
                    if textView == firstTView {
                        //In the array of text fields you want to set, if it is the top text field, deactivate it.
                        previousButton.isEnabled = false
                    } else {
                        //Other than the above
                        //Target the previous text field.
                        previousButton.target = objects[index - 1].obj
                        //Focus on the target.
                        previousButton.action = #selector(UITextField.becomeFirstResponder)
                    }
                    
                }else if let firstTField = objects.first?.obj as? UITextField {
                    if textField == firstTField {
                        //In the array of text fields you want to set, if it is the top text field, deactivate it.
                        previousButton.isEnabled = false
                    } else {
                        //Other than the above
                        //Target the previous text field.
                        previousButton.target = objects[index - 1].obj
                        //Focus on the target.
                        previousButton.action = #selector(UITextField.becomeFirstResponder)
                    }
                }
                
                ///Fixed space
                let fixedSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: self, action: nil)
                fixedSpace.width = 8
                
                let nextButton = UIBarButtonItem(image: imageDown, style: .plain, target: self, action: nil)
                nextButton.accessibilityIdentifier = "toolbar_go_down"
                
                if let lastTView = objects.last?.obj as? UITextView {
                    if textView == lastTView {
                        //Of the array of text fields you want to set, if it is the bottom text field, deactivate it.
                        nextButton.isEnabled = false
                    } else {
                        //Other than the above
                        //Set the next text field as the target.
                        nextButton.target = objects[index + 1].obj
                        //Focus on the target.
                        nextButton.action = #selector(UITextField.becomeFirstResponder)
                    }
                    
                }else if let lastTField = objects.last?.obj as? UITextField {
                    if textField == lastTField {
                        //Of the array of text fields you want to set, if it is the bottom text field, deactivate it.
                        nextButton.isEnabled = false
                    } else {
                        //Other than the above
                        //Set the next text field as the target.
                        nextButton.target = objects[index + 1].obj
                        //Focus on the target.
                        nextButton.action = #selector(UITextField.becomeFirstResponder)
                    }
                }
                
                //Add front and back buttons to bar button items.
                items.append(contentsOf: [previousButton, fixedSpace, nextButton])
            }
            
            // MARK:Done button settings
            let flexSpace = SwiftBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            
            //Add a Done button to the bar button item.
            let doneButton = SwiftBarButtonItem(title: object.doneButtonTitle ?? "Done", style: .done, actionHandler: nil)
            doneButton.accessibilityIdentifier = "toolbar_done"
            
            if let doneButtonClosure = object.doneButtonClosure {
                doneButton.actionHandler = doneButtonClosure
                
            }else {
                doneButton.actionHandler = { _ in
                    self.view.endEditing(false)
                }
            }
            
            var rightSideButtons = [flexSpace, doneButton]
            
            //Add a custom button to the bar button item.
            if let customButtonTitle = object.customButtonTitle, let customButtonClosure = object.customButtonClosure {
                let customButton = SwiftBarButtonItem(title: customButtonTitle, style: .plain, actionHandler: nil)
                customButton.actionHandler = customButtonClosure
                customButton.accessibilityIdentifier = "toolbar_custom_btn"
                
                rightSideButtons.insert(customButton, at: rightSideButtons.count - 1)
            }
            
            items.append(contentsOf: rightSideButtons)
            toolBar.setItems(items, animated: false)
            toolBar.sizeToFit()
            
            if let textView = textView {
                textView.inputAccessoryView = toolBar
            }else if let textField = textField {
                textField.inputAccessoryView = toolBar
            }
        }
    }
}
