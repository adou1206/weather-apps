//
//  Extension+UIStackView.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 25/03/2024.
//

import UIKit

extension UIStackView {
    var contentLayoutMargins: UIEdgeInsets {
        get { layoutMargins }
        set {
            layoutMargins = newValue
            isLayoutMarginsRelativeArrangement = true
        }
    }
}
