//
//  Extension+UIEdgeInsets.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 25/03/2024.
//

// refer: https://marlonjames.medium.com/padding-uistackviews-8e8a9c19e2ba

import UIKit

struct UIEdgeInsetBuilder {
    
    var top: CGFloat = 0
    var left: CGFloat = 0
    var bottom: CGFloat = 0
    var right: CGFloat = 0
    
    func top(_ value: CGFloat) -> Self {
        var builder = self
        builder.top = value
        
        return builder
    }
    
    func left(_ value: CGFloat) -> Self {
        var builder = self
        builder.left = value
        
        return builder
    }
    
    func bottom(_ value: CGFloat) -> Self {
        var builder = self
        builder.bottom = value
        
        return builder
    }
    
    func right(_ value: CGFloat) -> Self {
        var builder = self
        builder.right = value
        
        return builder
    }
    
    func all(_ value: CGFloat) -> Self {
        var builder = self
        builder.top = value
        builder.left = value
        builder.bottom = value
        builder.right = value
        
        return builder
    }
    
    func horizontal(_ value: CGFloat) -> Self {
        var builder = self
        builder.left = value
        builder.right = value
        
        return builder
    }
    
    func vertical(_ value: CGFloat) -> Self {
        var builder = self
        builder.top = value
        builder.bottom = value
        
        return builder
    }
    
    var insets: UIEdgeInsets {
        .init(
            top: top,
            left: left,
            bottom: bottom,
            right: right
        )
    }
}

// MARK: - Extension

extension UIEdgeInsets {
    static var set: UIEdgeInsetBuilder { .init() }
}
