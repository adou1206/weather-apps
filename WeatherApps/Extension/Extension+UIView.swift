//
//  Extension+UIView.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 18/06/2022.
//

import UIKit

extension UIView {
    ///get top anchor or safe area top anchor
    var safeTopAnchor: NSLayoutYAxisAnchor {
        return self.safeAreaLayoutGuide.topAnchor
    }
    
    ///get bottom anchor or safe area bottom anchor
    var safeBottomAnchor: NSLayoutYAxisAnchor {
        return self.safeAreaLayoutGuide.bottomAnchor
    }
    
    ///get leading anchor or safe area leading anchor
    var safeLeadingAnchor: NSLayoutXAxisAnchor {
        return self.safeAreaLayoutGuide.leadingAnchor
    }
    
    ///get trailing anchor or safe area trailing anchor
    var safeTrailingAnchor: NSLayoutXAxisAnchor {
        return self.safeAreaLayoutGuide.trailingAnchor
    }
    
    func addShadow(
        x: CGFloat,
        y: CGFloat,
        shadow_radius: CGFloat = 0,
        color: UIColor,
        opacity: Float = 0.5,
        scale: CGFloat = 1
    ) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = CGSize(width: x, height: y)
        layer.shadowRadius = shadow_radius
        
        layoutIfNeeded()
        
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale
    }
}
