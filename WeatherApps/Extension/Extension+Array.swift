//
//  Extension+Array.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 15/04/2024.
//

import Foundation

// refer: https://stackoverflow.com/a/45008042
extension Array where Element: Equatable {
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        guard let index = firstIndex(of: object) else {return}
        remove(at: index)
    }
}
