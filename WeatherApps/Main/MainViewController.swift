//
//  MainViewController.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 19/04/2024.
//

import UIKit

class MainViewController: UIViewController {
    
    private var selection_stack_view = UIStackView()
    private let selection_lists = ListStyle.allCases

    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }
}

extension MainViewController {
    private func initView() {
        view.backgroundColor = .white
        
        addSubView()
        stackViewUI()
    }
    
    private func addSubView() {
        view.addSubview(selection_stack_view)
    }
    
    private func stackViewUI() {
        selection_stack_view.translatesAutoresizingMaskIntoConstraints = false
        selection_stack_view.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        selection_stack_view.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        selection_stack_view.axis = .vertical
        selection_stack_view.spacing = 8
        selection_stack_view.alignment = .center
        selection_stack_view.distribution = .fillEqually
        
        setUpSelection()
    }
    
    private func setUpSelection() {
        for (index, selection_list) in selection_lists.enumerated() {
            let button = UIButton()

            selection_stack_view.addArrangedSubview(button)
            
            if #available(iOS 15.0, *) {
                // refer: https://medium.com/@2018.itsuki/ios-swift-remove-change-uibutton-paddings-3baaf25946b1
                var configuration = UIButton.Configuration.filled()
                configuration.contentInsets = NSDirectionalEdgeInsets(top: 12, leading: 25, bottom: 12, trailing: 25)
                configuration.baseBackgroundColor = .systemRed
                configuration.baseForegroundColor = .white
                configuration.cornerStyle = .large
                configuration.title = selection_list.titleText()
                button.configuration = configuration
                
            } else {
                button.backgroundColor = .systemRed
                button.contentEdgeInsets = UIEdgeInsets(top: 12, left: 25, bottom: 12, right: 25)
                button.layer.cornerRadius = 12
                button.setTitle(selection_list.titleText(), for: .normal)
                button.setTitleColor(.white, for: .normal)
            }

            button.tag = index
            button.addTarget(self, action: #selector(test), for: .touchUpInside)
        }
    }
    
    @objc private func test(_ sender: UIButton) {
        guard let nvc = self.navigationController else { return }
        
        nvc.pushViewController(selection_lists[sender.tag].vc, animated: true)
    }
}
