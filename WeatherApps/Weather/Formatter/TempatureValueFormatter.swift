//
//  TempatureValueFormatter.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 11/03/2024.
//

import Foundation
import DGCharts

public class TempatureValueFormatter: NSObject, AxisValueFormatter {
    private let public_method = PublicMethod.shared
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return public_method.makeTempatureDisplay(temp: value.rounded())
    }
}
