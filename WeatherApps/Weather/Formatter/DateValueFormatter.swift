//
//  DateValueFormatter.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 08/03/2024.
//

import Foundation
import DGCharts

public class DateValueFormatter: NSObject, AxisValueFormatter {
    private let public_method = PublicMethod.shared
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return public_method.convertUnitTimestampToDate(timestamp: value, format: "E", isShowNow: false)
    }
}
