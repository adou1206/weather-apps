//
//  File.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 18/06/2022.
//

import Foundation

//

struct WeatherModel: Decodable, Equatable {
    let timezone: String?
    let current: CurrentWeatherDetail?
    let hourly: [HourlyWeatherDetail]?
    let daily: [DailyWeatherDetail]?
}

struct CurrentWeatherDetail: Decodable, Equatable {
    let temp: Double?
    let feels_like: Double?
    let weather: [WeatherIcon]?
    let clouds: Int?
}

struct HourlyWeatherDetail: Decodable, Equatable {
    let dt: Int?
    let weather: [WeatherIcon]?
    let temp: Double?
    let clouds: Int?
}

struct DailyWeatherDetail: Decodable, Equatable {
    let dt: Int?
    let weather: [WeatherIcon]?
    let temp: DailyTemperature?
    let pop: Float?
}

struct DailyTemperature: Decodable, Equatable {
    let min: Double?
    let max: Double?
    let day: Double?
}

struct WeatherIcon: Decodable, Equatable {
    let main: String?
    let icon: String?
    
    func iconURL(size: iconSize = .medium) -> String? {
        guard let icon = icon else { return nil }

        return "http://openweathermap.org/img/wn/\(icon)\(size.rawValue).png"
    }
}

enum iconSize: String {
    case normal = ""
    case medium = "@2x"
    case big = "@4x"
}
