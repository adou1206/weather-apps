//
//  ViewController.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 16/06/2022.
//

import UIKit
import CoreLocation
import Combine
import DGCharts

/*
 
 MARK: Change Weather API to another free API cause OpenWeather after June Free will close (Try https://www.visualcrossing.com/)
 
*/

class WeatherViewController: UIViewController {
    private let public_method = PublicMethod.shared
    
    private let location_manager = CLLocationManager()
    
    private var hourly_collection_view = HourlyCollectionView()
    
    private var this_week_line_chart_view = LineChartView()
    
    private var this_week_list_background_view = ShadowOnView()
    private var chart_btn_background_view = ShadowOnView()
    private var collection_btn_background_view  = ShadowOnView()
    
    private var today_details_stack_view = UIStackView()
    private var this_week_stack_view = UIStackView()
    private var this_week_list_stack_view = UIStackView()
    
    private var top_background_view = UIView()
    private var bottom_background_view = UIView()
    
    private var today_label = UILabel()
    private var location_name_label = UILabel()
    private var current_tempature_label = UILabel()
    private var today_details_label = UILabel()
    private var this_week_label = UILabel()
    
    private var today_weather_icon_image_view = UIImageView()
    
    private var show_this_week_chart_btn = UIButton()
    private var show_this_week_collection_btn = UIButton()
    
    private var current_tempature: Double = 0.0
    private var hourly_weather_list = [HourlyWeatherDetail]()
    private var daily_weather_list = [DailyWeatherDetail]()
    
    private var vm: WeatherViewModel
    
    private var isLoading: Bool = false {
        didSet {
            isLoading ? showSpinner() : removeSpinner()
        }
    }
    
    private var bindings = Set<AnyCancellable>()
    
//    init(vm: WeatherViewModel = WeatherViewModel()) {
//        self.vm = vm
//        super.init(nibName: nil, bundle: nil)
//    }
    
    init() {
        self.vm = WeatherViewModel()
        super.init(nibName: nil, bundle: nil)
        
        location_manager.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if let nvc = navigationController as? CustomNavigationController {
            nvc.list_style = .weather_style
        }
        
        initView()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        
//        updateDisplay()
//    }
}

extension WeatherViewController {
    func initView() {
//        navigationItem.title = ListStyle.weather_style.titleText()
//        view.layer.addSublayer(ListStyle.weather_style.gradientLayer(in: view.frame))
        view.backgroundColor = .systemBackground
        
        navigationItem.title = ListStyle.weather_style.titleText()

        addSubView()
        viewUI()
        labelViewUI()
        imageViewUI()
        stackViewUI()
        buttonViewUI()
        collectionViewUI()
        lineChartViewUI()
        vm.$isLoading.assign(to: \.isLoading, on: self).store(in: &bindings)
        
        show_this_week_chart_btn.sendActions(for: .touchUpInside)
    }
    
    func addSubView() {
        view.addSubview(top_background_view)
        view.addSubview(hourly_collection_view)
        view.addSubview(this_week_stack_view)
        view.addSubview(bottom_background_view)
        
        top_background_view.addSubview(today_details_stack_view)
        
        bottom_background_view.addSubview(this_week_line_chart_view)
        bottom_background_view.addSubview(this_week_list_background_view)
        
        this_week_list_background_view.addSubview(this_week_list_stack_view)
        
        today_details_stack_view.addArrangedSubview(today_label)
        today_details_stack_view.addArrangedSubview(location_name_label)
        today_details_stack_view.addArrangedSubview(today_weather_icon_image_view)
        today_details_stack_view.addArrangedSubview(current_tempature_label)
        today_details_stack_view.addArrangedSubview(today_details_label)
        
        this_week_stack_view.addArrangedSubview(this_week_label)
        this_week_stack_view.addArrangedSubview(chart_btn_background_view)
        this_week_stack_view.addArrangedSubview(collection_btn_background_view)
        
        chart_btn_background_view.addSubview(show_this_week_chart_btn)
        
        collection_btn_background_view.addSubview(show_this_week_collection_btn)
    }
    
    func viewUI() {
        top_background_view.translatesAutoresizingMaskIntoConstraints = false
        top_background_view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        top_background_view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        top_background_view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        bottom_background_view.translatesAutoresizingMaskIntoConstraints = false
        bottom_background_view.topAnchor.constraint(equalTo: this_week_stack_view.bottomAnchor, constant: 20).isActive = true
        bottom_background_view.bottomAnchor.constraint(equalTo: view.safeBottomAnchor).isActive = true
        bottom_background_view.leadingAnchor.constraint(equalTo: today_details_label.leadingAnchor).isActive = true
        bottom_background_view.trailingAnchor.constraint(equalTo: today_details_label.trailingAnchor).isActive = true
        bottom_background_view.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.24).isActive = true
        bottom_background_view.backgroundColor = .clear
        
        chart_btn_background_view.translatesAutoresizingMaskIntoConstraints = false
        chart_btn_background_view.widthAnchor.constraint(equalToConstant: 50).isActive = true
        chart_btn_background_view.backgroundColor = ColorSets.white_text.color()
        chart_btn_background_view.layer.cornerRadius = 12
        
        collection_btn_background_view.translatesAutoresizingMaskIntoConstraints = false
        collection_btn_background_view.widthAnchor.constraint(equalToConstant: 50).isActive = true
        collection_btn_background_view.backgroundColor = ColorSets.white_text.color()
        collection_btn_background_view.layer.cornerRadius = 12
        
        this_week_list_background_view.translatesAutoresizingMaskIntoConstraints = false
        this_week_list_background_view.topAnchor.constraint(equalTo: bottom_background_view.topAnchor).isActive = true
        this_week_list_background_view.bottomAnchor.constraint(equalTo: bottom_background_view.bottomAnchor).isActive = true
        this_week_list_background_view.leadingAnchor.constraint(equalTo: bottom_background_view.leadingAnchor).isActive = true
        this_week_list_background_view.trailingAnchor.constraint(equalTo: bottom_background_view.trailingAnchor).isActive = true
        this_week_list_background_view.backgroundColor = ColorSets.white_text.color()
        this_week_list_background_view.layer.cornerRadius = 12
    }
    
    func stackViewUI() {
        today_details_stack_view.translatesAutoresizingMaskIntoConstraints = false
        today_details_stack_view.topAnchor.constraint(equalTo: top_background_view.safeTopAnchor).isActive = true
        today_details_stack_view.bottomAnchor.constraint(equalTo: top_background_view.bottomAnchor, constant: -75).isActive = true
        today_details_stack_view.leadingAnchor.constraint(equalTo: top_background_view.leadingAnchor, constant: 22).isActive = true
        today_details_stack_view.trailingAnchor.constraint(equalTo: top_background_view.trailingAnchor, constant: -22).isActive = true
        today_details_stack_view.axis = .vertical
        today_details_stack_view.distribution = .fill
        today_details_stack_view.spacing = 6
        today_details_stack_view.backgroundColor = .clear
        
        this_week_stack_view.translatesAutoresizingMaskIntoConstraints = false
        this_week_stack_view.topAnchor.constraint(equalTo: hourly_collection_view.bottomAnchor, constant: 10).isActive = true
        this_week_stack_view.leadingAnchor.constraint(equalTo: today_details_label.leadingAnchor).isActive = true
        this_week_stack_view.trailingAnchor.constraint(equalTo: today_details_label.trailingAnchor).isActive = true
        this_week_stack_view.heightAnchor.constraint(equalToConstant: 40).isActive = true
        this_week_stack_view.axis = .horizontal
        this_week_stack_view.distribution = .fillProportionally
        this_week_stack_view.spacing = 12
        this_week_stack_view.backgroundColor = .clear
        
        this_week_list_stack_view.translatesAutoresizingMaskIntoConstraints = false
        this_week_list_stack_view.topAnchor.constraint(equalTo: this_week_list_background_view.topAnchor).isActive = true
        this_week_list_stack_view.bottomAnchor.constraint(equalTo: this_week_list_background_view.bottomAnchor).isActive = true
        this_week_list_stack_view.leadingAnchor.constraint(equalTo: this_week_list_background_view.leadingAnchor).isActive = true
        this_week_list_stack_view.trailingAnchor.constraint(equalTo: this_week_list_background_view.trailingAnchor).isActive = true
        this_week_list_stack_view.contentLayoutMargins = .set.top(24).bottom(10).horizontal(10).insets
        this_week_list_stack_view.axis = .horizontal
        this_week_list_stack_view.distribution = .fillProportionally
        this_week_list_stack_view.backgroundColor = .clear
    }
    
    func lineChartViewUI() {
        this_week_line_chart_view.translatesAutoresizingMaskIntoConstraints = false
        this_week_line_chart_view.topAnchor.constraint(equalTo: bottom_background_view.topAnchor, constant: -20).isActive = true
        this_week_line_chart_view.bottomAnchor.constraint(equalTo: bottom_background_view.bottomAnchor).isActive = true
        this_week_line_chart_view.leadingAnchor.constraint(equalTo: bottom_background_view.leadingAnchor).isActive = true
        this_week_line_chart_view.trailingAnchor.constraint(equalTo: bottom_background_view.trailingAnchor).isActive = true
        this_week_line_chart_view.noDataText = "No This Week Average Tempature Data Display"
        this_week_line_chart_view.noDataTextColor = ColorSets.black_text.color()!
        this_week_line_chart_view.noDataFont = .montserratRegular(size: 18)
        this_week_line_chart_view.noDataTextAlignment = .center
        // refer: https://github.com/ChartsOrg/Charts/issues/4023
        this_week_line_chart_view.extraRightOffset = 15
        this_week_line_chart_view.delegate = self
        
        let left_axis = this_week_line_chart_view.leftAxis
        left_axis.labelFont = .montserratMedium(size: 14)
        left_axis.labelTextColor = (ColorSets.black_text.color()?.withAlphaComponent(0.3))!
        left_axis.valueFormatter = TempatureValueFormatter()
        
        let x_axis = this_week_line_chart_view.xAxis
        x_axis.labelPosition = .bottom
        x_axis.labelFont = .montserratMedium(size: 14)
        x_axis.labelTextColor = (ColorSets.black_text.color()?.withAlphaComponent(0.3))!
        x_axis.drawGridLinesEnabled = false
        x_axis.setLabelCount(7, force: true)
        x_axis.yOffset = 10
        x_axis.valueFormatter = DateValueFormatter()

        
        let balloon_marker = BalloonMarker(color: .systemBlue,
                                   font: .montserratRegular(size: 12),
                                   textColor: .white,
                                   insets: UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        )
        balloon_marker.chartView = this_week_line_chart_view
        balloon_marker.minimumSize = CGSize(width: 40, height: 40)
        this_week_line_chart_view.marker = balloon_marker
        
        this_week_line_chart_view.rightAxis.enabled = false
        this_week_line_chart_view.animate(xAxisDuration: 2.5)
    }
    
    func labelViewUI() {
        today_label.translatesAutoresizingMaskIntoConstraints = false
        today_label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        today_label.font = .montserratRegular(size: 18)
        today_label.textAlignment = .left
        today_label.numberOfLines = 0
        today_label.text = public_method.convertUnitTimestampToDate(
            timestamp: Date().timeIntervalSince1970,
            format: "EEEE, d MMMM",
            isShowNow: false
        )
        
        location_name_label.translatesAutoresizingMaskIntoConstraints = false
        location_name_label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        location_name_label.font = .montserratBold(size: 22)
        location_name_label.textAlignment = .left
        location_name_label.numberOfLines = 0
        
        current_tempature_label.translatesAutoresizingMaskIntoConstraints = false
        current_tempature_label.heightAnchor.constraint(equalToConstant: 50).isActive = true
        current_tempature_label.font = .montserratBold(size: 32)
        current_tempature_label.textAlignment = .center
        current_tempature_label.numberOfLines = 0
        
        today_details_label.translatesAutoresizingMaskIntoConstraints = false
        today_details_label.heightAnchor.constraint(equalToConstant: 65).isActive = true
        today_details_label.font = .montserratRegular(size: 18)
        today_details_label.textAlignment = .center
        today_details_label.numberOfLines = 0
        
        this_week_label.font = .montserratMedium(size: 16)
        this_week_label.textColor = ColorSets.black_text.color()
        this_week_label.textAlignment = .left
        this_week_label.text = "This Week"
    }
    
    private func buttonViewUI() {
        show_this_week_chart_btn.translatesAutoresizingMaskIntoConstraints = false
        show_this_week_chart_btn.topAnchor.constraint(equalTo: chart_btn_background_view.topAnchor).isActive = true
        show_this_week_chart_btn.bottomAnchor.constraint(equalTo: chart_btn_background_view.bottomAnchor).isActive = true
        show_this_week_chart_btn.leadingAnchor.constraint(equalTo: chart_btn_background_view.leadingAnchor).isActive = true
        show_this_week_chart_btn.trailingAnchor.constraint(equalTo: chart_btn_background_view.trailingAnchor).isActive = true
        show_this_week_chart_btn.backgroundColor = ColorSets.white_text.color()
        show_this_week_chart_btn.setImage(
            UIImage(
                systemName: "chart.xyaxis.line"
            )?.withTintColor(
                .systemBlue,
                renderingMode: .alwaysOriginal
            ),
            for: .selected
        )
        show_this_week_chart_btn.setImage(
            UIImage(
                systemName: "chart.xyaxis.line"
            )?.withTintColor(
                .systemGray,
                renderingMode: .alwaysOriginal
            ),
            for: .normal
        )
        show_this_week_chart_btn.layer.cornerRadius = 12
        show_this_week_chart_btn.addTarget(self, action: #selector(showChartButtonAction), for: .touchUpInside)
        
        show_this_week_collection_btn.translatesAutoresizingMaskIntoConstraints = false
        show_this_week_collection_btn.topAnchor.constraint(equalTo: collection_btn_background_view.topAnchor).isActive = true
        show_this_week_collection_btn.bottomAnchor.constraint(equalTo: collection_btn_background_view.bottomAnchor).isActive = true
        show_this_week_collection_btn.leadingAnchor.constraint(equalTo: collection_btn_background_view.leadingAnchor).isActive = true
        show_this_week_collection_btn.trailingAnchor.constraint(equalTo: collection_btn_background_view.trailingAnchor).isActive = true
        show_this_week_collection_btn.backgroundColor = ColorSets.white_text.color()
        // refer: https://sarunw.com/posts/how-to-change-uiimage-color-in-swift/
        show_this_week_collection_btn.setImage(
            UIImage(
                systemName: "list.bullet.rectangle.portrait"
            )?.withTintColor(
                .systemBlue,
                renderingMode: .alwaysOriginal
            ),
            for: .selected
        )
        show_this_week_collection_btn.setImage(
            UIImage(
                systemName: "list.bullet.rectangle.portrait"
            )?.withTintColor(
                .systemGray,
                renderingMode: .alwaysOriginal
            ),
            for: .normal
        )
        show_this_week_collection_btn.layer.cornerRadius = 12
        show_this_week_collection_btn.addTarget(self, action: #selector(showCollectionButtonAction), for: .touchUpInside)
    }
    
    private func imageViewUI() {
        today_weather_icon_image_view.backgroundColor = .clear
        today_weather_icon_image_view.contentMode = .scaleAspectFit
    }
    
    func collectionViewUI() {
        hourly_collection_view.translatesAutoresizingMaskIntoConstraints = false
        hourly_collection_view.centerYAnchor.constraint(equalTo: top_background_view.bottomAnchor, constant: 8).isActive = true
        hourly_collection_view.leadingAnchor.constraint(equalTo: today_details_label.leadingAnchor).isActive = true
        hourly_collection_view.trailingAnchor.constraint(equalTo: today_details_label.trailingAnchor).isActive = true
        hourly_collection_view.heightAnchor.constraint(equalToConstant: 140).isActive = true
        hourly_collection_view.showsVerticalScrollIndicator = false
        hourly_collection_view.showsHorizontalScrollIndicator = false
        hourly_collection_view.backgroundColor = .clear
    }
    
    func updateDisplay() {
        guard let vm = vm.weatherModel else { return }
        
        if let current = vm.current, let current_temp = current.temp {
            current_tempature_label.text = public_method.makeTempatureDisplay(temp: current_temp.rounded())
            
            if let current_clouds = current.clouds,
               let current_weather = current.weather,
               let current_weather_first = current_weather.first,
               let current_weather_main = current_weather_first.main,
               let current_weather_icon_url = current_weather_first.iconURL(size: .medium),
               let daily = vm.daily,
               let daily_first = daily.first,
               let daily_first_temp = daily_first.temp,
               let daily_first_temp_max  = daily_first_temp.max,
               let daily_first_temp_min  = daily_first_temp.min,
               let current_feels_like = current.feels_like
            {
                let weather_type = WeatherType.weatherTypeFromString(
                    main: current_weather_main,
                    clouds: current_clouds
                )
                
                top_background_view.backgroundColor = weather_type.displayColor()
                
                today_label.textColor = weather_type.displayTextColor()
                location_name_label.textColor = weather_type.displayTextColor()
                current_tempature_label.textColor = weather_type.displayTextColor()
                today_details_label.textColor = weather_type.displayTextColor()
                
                today_details_label.text = "\(weather_type.displayText()) | H: \(public_method.makeTempatureDisplay(temp: daily_first_temp_max.rounded())) L: \(public_method.makeTempatureDisplay(temp: daily_first_temp_min.rounded())) | feels like \(public_method.makeTempatureDisplay(temp: current_feels_like.rounded()))"
                
                _ = today_weather_icon_image_view.loadFrom(URLAddress: current_weather_icon_url)
            }
        }
        
        if let hourly = vm.hourly {
            var calendar = Calendar.current
            calendar.locale = Locale.current
            calendar.timeZone = .current
            
            if let tomorrow = calendar.date(byAdding: .day, value: 1, to: Date()) {
                hourly_weather_list = hourly.filter({
                    detail in
                    if let dt = detail.dt {
                        return TimeInterval(
                            dt
                        ) < tomorrow.timeIntervalSince1970
                        }
                        
                        return false
                    })
            }
            
            hourly_collection_view.hourly_weather_list = hourly
        }
        
        if let dailys = vm.daily {
            // this is the Array that will eventually be displayed on the graph
            var line_chart_entry = [ChartDataEntry]()
            
            for daily in dailys.dropLast() {
                // this week stack view set up
                this_week_list_stack_view.addArrangedSubview(
                    ThisWeekDetailsStackView(daily_weather_detail: daily, is_first: this_week_list_stack_view.arrangedSubviews.count == 0)
                )
                // end this week stack view set up
                
                if let daily_dt = daily.dt,
                   let daily_temp = daily.temp,
                   let daily_temp_day = daily_temp.day {
                    line_chart_entry.append(
                        ChartDataEntry(
                            x: Double(daily_dt),
                            y: daily_temp_day
                        )
                    )
                }
            }
            
            let line = LineChartDataSet(entries: line_chart_entry, label: "")
            
            line.mode = .linear
            line.colors = [.systemBlue]
            line.drawValuesEnabled = false
            line.lineWidth = 2.0
            line.formSize = 0
            line.circleColors = [.systemBlue]
            line.circleHoleRadius = 0
            line.circleRadius = 4
            line.drawFilledEnabled = true
            
            let gradient = CGGradient(
                colorsSpace: nil,
                colors: [
                    UIColor.white.cgColor,
                    UIColor.systemBlue.cgColor
                ] as CFArray,
                locations: nil
            )!
            
            line.fill = LinearGradientFill(gradient: gradient, angle: 90)
            line.fillAlpha = 0.11
            
            let line_data = LineChartData(dataSet: line)
            
            this_week_line_chart_view.data = line_data
        }
    }
    
    func locationOrPermissionDeniedAction() {
        Task {
            location_name_label.text = try await public_method.showAddressWithCoordinate(
                location: CLLocation(
                    latitude: 33.44,
                    longitude: -94.04
                )
            )
            
            await vm.fetchWeather(for: nil)
        }
    }
    
    @objc func showChartButtonAction() {
        if !show_this_week_chart_btn.isSelected {
            show_this_week_chart_btn.isSelected = true
            show_this_week_collection_btn.isSelected = false
            
            this_week_line_chart_view.isHidden = false
            this_week_list_background_view.isHidden = true
        }
    }
    
    @objc func showCollectionButtonAction() {
        if !show_this_week_collection_btn.isSelected {
            show_this_week_chart_btn.isSelected = false
            show_this_week_collection_btn.isSelected = true
            
            this_week_line_chart_view.isHidden = true
            this_week_list_background_view.isHidden = false
        }
    }
}

extension WeatherViewController: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if #available(iOS 14.0, *) {
            switch manager.authorizationStatus {
            case .notDetermined:
                location_manager.requestAlwaysAuthorization()
                location_manager.requestWhenInUseAuthorization()
                break
                
            case .authorizedWhenInUse, .authorizedAlways:
                location_manager.requestLocation()
                break
                
            default:
                locationOrPermissionDeniedAction()
                break
            }
        } else {
            // Fallback on earlier versions
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                location_manager.requestAlwaysAuthorization()
                location_manager.requestWhenInUseAuthorization()
                break
                
            case .authorizedWhenInUse, .authorizedAlways:
                location_manager.requestLocation()
                break
                
            default:
                locationOrPermissionDeniedAction()
                break
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            Task {
                do {
                    location_name_label.text = try await public_method.showAddressWithCoordinate(location: location)
                    
                }catch {
                    alertWithTitle("Notice", message: error.localizedDescription)
                }
                
                defer {
                    if vm.hasError {
                        alertWithTitle("Notice", message: vm.error?.errorDescription ?? "")
                        
                    }else {
                        updateDisplay()
                    }
                }
                
                await vm.fetchWeather(for: location)
            }
        }else {
            locationOrPermissionDeniedAction()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}

extension WeatherViewController: ChartViewDelegate {
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        print("chartValueNothingSelected")
    }
    
    func chartViewDidEndPanning(_ chartView: ChartViewBase) {
        print("chartViewDidEndPanning")
    }
    
    func chartTranslated(_ chartView: ChartViewBase, dX: CGFloat, dY: CGFloat) {
        print("chartTranslated")
    }
    
    func chartView(_ chartView: ChartViewBase, animatorDidStop animator: Animator) {
        print("chartView")
    }
    
    func chartScaled(_ chartView: ChartViewBase, scaleX: CGFloat, scaleY: CGFloat) {
        print("chartScaled")
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print("chartValueSelected")
    }
}
