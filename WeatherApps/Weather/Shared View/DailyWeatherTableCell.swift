//
//  DailyWeatherTableCell.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 18/06/2022.
//

import UIKit

// Noted: this is an unused class.
class DailyWeatherTableCell: UITableViewCell {
    private var public_method = PublicMethod.shared
    
    private var background_view = UIView()
    private var underline_view = UIView()
    
    private var stack_view = UIStackView()
    private var detail_stack_view = UIStackView()
    private var weather_stack_view = UIStackView()
    
    private var title_label = UILabel()
    private var precipitations_label = UILabel()
    private var max_min_tempature_label = UILabel()
    private var weather_label = UILabel()
    
    private var icon_image_view = UIImageView()
    
    private var image_request: Cancellable?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        title_label.text = nil
        max_min_tempature_label.text = nil
        precipitations_label.text = nil
        weather_label.text = nil
        
        icon_image_view.image = nil
        
        
        if let image_request = image_request {
            image_request.cancel()
        }
    }
    
    func configure(view_model: DailyWeatherDetail) {
        if let dt = view_model.dt {
            title_label.text = public_method.convertUnitTimestampToDate(timestamp: TimeInterval(dt))
        }
        
        if let temp = view_model.temp, let max = temp.max, let min = temp.min {
            max_min_tempature_label.text = "Temperature (Min - Max): \(public_method.makeTempatureDisplay(temp: min.rounded())) - \(public_method.makeTempatureDisplay(temp: max.rounded()))"
        }
        
        if let pop = view_model.pop {
            precipitations_label.text = "Probability of precipitation (%): \(String(format: "%.2f", pop * 100))"
        }
        
        if let weather = view_model.weather, let weather_last = weather.last, let icon_url = weather_last.iconURL(size: .normal) {
            weather_label.text = weather_last.main
            image_request = icon_image_view.loadFrom(URLAddress: icon_url)
        }
    }
}

extension DailyWeatherTableCell {
    private func initView() {
        backgroundColor = .clear
        selectionStyle = .none
        
        addSubView()
        viewUI()
        labelViewUI()
        stackViewUI()
        imageViewUI()
    }
    
    private func addSubView() {
        contentView.addSubview(background_view)
        
        background_view.addSubview(title_label)
        background_view.addSubview(stack_view)
        background_view.addSubview(underline_view)
        
        stack_view.addArrangedSubview(detail_stack_view)
        stack_view.addArrangedSubview(weather_stack_view)
        
        detail_stack_view.addArrangedSubview(max_min_tempature_label)
        detail_stack_view.addArrangedSubview(precipitations_label)
        
        weather_stack_view.addArrangedSubview(weather_label)
        weather_stack_view.addArrangedSubview(icon_image_view)
    }
    
    private func viewUI() {
        background_view.translatesAutoresizingMaskIntoConstraints = false
        background_view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        background_view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        background_view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        background_view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        background_view.backgroundColor = .clear
        
        underline_view.translatesAutoresizingMaskIntoConstraints = false
        underline_view.topAnchor.constraint(equalTo: stack_view.bottomAnchor, constant: 10).isActive = true
        underline_view.bottomAnchor.constraint(equalTo: background_view.bottomAnchor, constant: -20).isActive = true
        underline_view.leadingAnchor.constraint(equalTo: stack_view.leadingAnchor).isActive = true
        underline_view.trailingAnchor.constraint(equalTo: stack_view.trailingAnchor).isActive = true
        underline_view.heightAnchor.constraint(equalToConstant: 1).isActive = true
        underline_view.backgroundColor = .systemGray5
    }
    
    private func stackViewUI() {
        stack_view.translatesAutoresizingMaskIntoConstraints = false
        stack_view.topAnchor.constraint(equalTo: title_label.bottomAnchor, constant: 5).isActive = true
        stack_view.leadingAnchor.constraint(equalTo: title_label.leadingAnchor).isActive = true
        stack_view.trailingAnchor.constraint(equalTo: title_label.trailingAnchor).isActive = true
        stack_view.backgroundColor = .clear
        stack_view.axis = .horizontal
        stack_view.spacing = 20
        stack_view.distribution = .fillProportionally
        
        detail_stack_view.backgroundColor = .clear
        detail_stack_view.axis = .vertical
        detail_stack_view.spacing = 8
        detail_stack_view.distribution = .fillEqually
        
        weather_stack_view.backgroundColor = .clear
        weather_stack_view.axis = .vertical
        weather_stack_view.distribution = .fill
        weather_stack_view.alignment = .center
    }
    
    private func labelViewUI() {
        title_label.translatesAutoresizingMaskIntoConstraints = false
        title_label.topAnchor.constraint(equalTo: background_view.topAnchor).isActive = true
        title_label.leadingAnchor.constraint(equalTo: background_view.leadingAnchor).isActive = true
        title_label.trailingAnchor.constraint(equalTo: background_view.trailingAnchor).isActive = true
        title_label.font = .montserratBold(size: 18)
        title_label.textColor = ColorSets.black_text.color()
        title_label.numberOfLines = 0
        
        weather_label.font = .montserratRegular(size: 16)
        weather_label.textAlignment = .center
        weather_label.textColor = ColorSets.black_text.color()
        weather_label.lineBreakMode = .byWordWrapping

        max_min_tempature_label.font = .montserratRegular(size: 16)
        max_min_tempature_label.textColor = ColorSets.black_text.color()
        max_min_tempature_label.numberOfLines = 0

        precipitations_label.font = .montserratRegular(size: 16)
        precipitations_label.textColor = ColorSets.black_text.color()
        precipitations_label.numberOfLines = 0
    }
    
    private func imageViewUI() {
        icon_image_view.backgroundColor = .clear
        icon_image_view.contentMode = .scaleAspectFit
    }
}
