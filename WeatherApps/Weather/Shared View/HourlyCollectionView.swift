//
//  HourlyCollectionView.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 19/06/2022.
//

import UIKit

class HourlyCollectionView: UICollectionView {
    var hourly_weather_list = [HourlyWeatherDetail]() {
        didSet {
            reloadData()
        }
    }
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        let customLayout = UICollectionViewCompositionalLayout { (sectionNumber, env) in
            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1))
            let item = NSCollectionLayoutItem(layoutSize: itemSize)
            
            let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.16), heightDimension: .fractionalHeight(1))
            let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 1)
            
            let section = NSCollectionLayoutSection(group: group)
            section.interGroupSpacing = 10
            section.orthogonalScrollingBehavior = .continuousGroupLeadingBoundary
            section.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0)
            
            return section
        }
        
        super.init(frame: frame, collectionViewLayout: customLayout)
        
        initView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension HourlyCollectionView {
    private func initView() {
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        bounces = false
        register(HourlyCollectionCell.self, forCellWithReuseIdentifier: HourlyCollectionCell.identifier)
        dataSource = self
        delegate = self
    }
}

extension HourlyCollectionView: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hourly_weather_list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HourlyCollectionCell.identifier, for: indexPath) as! HourlyCollectionCell
        cell.configure(view_model: hourly_weather_list[indexPath.row])
        
        return cell
    }
}
