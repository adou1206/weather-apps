//
//  ShadowOnView.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 25/03/2024.
//

import UIKit

//struct ShadowSetUpViewModel {
//    let shadow_offset: CGSize
//    let shadow_color: UIColor
//    var shadow_radius: CGFloat = 0
//    var shadow_corner_radius: CGFloat = 0.0
//    var shadow_opacity: Float = 0.5
//    var scale: CGFloat = 1.0
//}

class ShadowOnView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addShadow(
            x: 1,
            y: 1,
            shadow_radius: 3,
            color: ColorSets.black_text.color()!,
            opacity: 0.3,
            scale: PublicStaticData.window_scene_screen_scale
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //    init(view_model: ShadowSetUpViewModel) {
    //        super.init(frame: .zero)
    //
    //        layer.cornerRadius = view_model.shadow_corner_radius
    //        layer.masksToBounds = false
    //        layer.shadowColor = view_model.shadow_color.cgColor
    //        layer.shadowOpacity = view_model.shadow_opacity
    //        layer.shadowOffset = view_model.shadow_offset
    //        layer.shadowRadius = view_model.shadow_radius
    //
    //        layoutIfNeeded()
    //
    //        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: view_model.shadow_corner_radius).cgPath
    //        layer.shouldRasterize = true
    //        layer.rasterizationScale = view_model.scale
    //    }
        
}
