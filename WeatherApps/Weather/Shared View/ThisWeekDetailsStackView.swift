//
//  ThisWeekDetailsStackView.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 22/03/2024.
//

import UIKit

class ThisWeekDetailsStackView: UIStackView {
    private let public_method = PublicMethod.shared
    
    private var date_label = UILabel()
    private var tempature_label = UILabel()
    private var day_label = UILabel()
    
    private var icon_image_view = UIImageView()
    
    private let daily_weather_detail: DailyWeatherDetail
    private var is_first: Bool
    
    init(daily_weather_detail: DailyWeatherDetail, is_first: Bool) {
        self.daily_weather_detail = daily_weather_detail
        self.is_first = is_first
        
        super.init(frame: .zero)
        
        initView()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ThisWeekDetailsStackView {
    private func initView() {
        axis = .vertical
        distribution = .fillProportionally
        spacing = 4
        backgroundColor = .clear
        
        addSubView()
        labelViewUI()
        imageViewUI()
        configure()
    }
    
    private func addSubView() {
        addArrangedSubview(date_label)
        addArrangedSubview(tempature_label)
        addArrangedSubview(icon_image_view)
        addArrangedSubview(day_label)
    }
    
    private func labelViewUI() {
        date_label.font = .montserratBold(size: 13)
        date_label.textColor = ColorSets.black_text.color()
        date_label.textAlignment = .center
        date_label.lineBreakMode = .byWordWrapping

        tempature_label.font = .montserratBold(size: 13)
        tempature_label.textColor = ColorSets.black_text.color()
        tempature_label.textAlignment = .center
        tempature_label.lineBreakMode = .byWordWrapping
        
        day_label.font = .montserratBold(size: 13)
        day_label.textColor = ColorSets.black_text.color()
        day_label.textAlignment = .center
        day_label.lineBreakMode = .byWordWrapping
    }
    
    private func imageViewUI() {
        icon_image_view.backgroundColor = .clear
        icon_image_view.contentMode = .scaleAspectFit
    }
    
    private func configure() {
        if let dt = daily_weather_detail.dt {
            date_label.text = public_method.convertUnitTimestampToDate(
                timestamp: TimeInterval(dt),
                format: is_first ? "MMM dd" : "dd",
                isShowNow: false
            )
            day_label.text = public_method.convertUnitTimestampToDate(
                timestamp: TimeInterval(dt),
                format: "E",
                isShowNow: false
            )
        }
        
        if let temp = daily_weather_detail.temp,
           let day = temp.day {
            tempature_label.text = public_method.makeTempatureDisplay(temp: day.rounded())
        }
        
        if let weather = daily_weather_detail.weather,
           let weather_last = weather.last,
           let icon_url = weather_last.iconURL(size: .normal) {
            _ = icon_image_view.loadFrom(URLAddress: icon_url)
        }
    }
}
