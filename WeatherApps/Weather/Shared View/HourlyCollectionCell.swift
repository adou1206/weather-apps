//
//  HourlyCollectionCell.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 19/06/2022.
//

import UIKit

class HourlyCollectionCell: UICollectionViewCell {
    static let identifier = "HourlyCollectionCell"
    
    private let public_method = PublicMethod.shared
    
    private var background_view = ShadowOnView()
    
    private var stack_view = UIStackView()
    
    private var time_label = UILabel()
    private var tempature_label = UILabel()
    
    private var icon_image_view = UIImageView()
    
    private var image_request: Cancellable?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        time_label.text = nil
        tempature_label.text = nil
        
        icon_image_view.image = nil
        
        if let image_request = image_request {
            image_request.cancel()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(view_model: HourlyWeatherDetail) {
        if let dt = view_model.dt {
            time_label.text = public_method.convertUnitTimestampToDate(timestamp: TimeInterval(dt), format: "HH")
        }
        
        if let temp = view_model.temp {
            tempature_label.text = public_method.makeTempatureDisplay(temp: temp.rounded())
        }
        
        if let weather = view_model.weather,
           let weather_last = weather.last,
           let icon_url = weather_last.iconURL(size: .normal) {
            image_request = icon_image_view.loadFrom(URLAddress: icon_url)
        }
    }
}

extension HourlyCollectionCell {
    private func initView() {
        backgroundColor = .clear
        
        addSubView()
        viewUI()
        stackViewUI()
        labelViewUI()
        imageViewUI()
    }
    
    private func addSubView() {
        contentView.addSubview(background_view)
        
        background_view.addSubview(stack_view)
        
        stack_view.addArrangedSubview(time_label)
        stack_view.addArrangedSubview(icon_image_view)
        stack_view.addArrangedSubview(tempature_label)
    }
    
    private func viewUI() {
        background_view.translatesAutoresizingMaskIntoConstraints = false
        background_view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        background_view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        background_view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        background_view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        background_view.layer.cornerRadius = 12
        background_view.backgroundColor = ColorSets.white_text.color()
    }
    
    private func stackViewUI() {
        stack_view.translatesAutoresizingMaskIntoConstraints = false
        stack_view.topAnchor.constraint(equalTo: background_view.topAnchor, constant: 8).isActive = true
        stack_view.bottomAnchor.constraint(equalTo:  background_view.bottomAnchor, constant: -8).isActive = true
        stack_view.leadingAnchor.constraint(equalTo: background_view.leadingAnchor).isActive = true
        stack_view.trailingAnchor.constraint(equalTo: background_view.trailingAnchor).isActive = true
        stack_view.axis = .vertical
        stack_view.distribution = .fillProportionally
        stack_view.spacing = 8
        stack_view.backgroundColor = .clear
    }
    
    private func labelViewUI() {
        time_label.font = .montserratMedium(size: 16)
        time_label.textColor = ColorSets.black_text.color()
        time_label.textAlignment = .center
        time_label.lineBreakMode = .byWordWrapping

        tempature_label.font = .montserratMedium(size: 16)
        tempature_label.textColor = ColorSets.black_text.color()
        tempature_label.textAlignment = .center
        tempature_label.lineBreakMode = .byWordWrapping
    }
    
    private func imageViewUI() {
        icon_image_view.backgroundColor = .clear
        icon_image_view.contentMode = .scaleAspectFit
    }
}
