//
//  ShadowOnButton.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 22/03/2024.
//

// refer: https://blog.eclypse.io/how-to-add-shadow-to-a-uibutton-d5b909ec72ee & https://github.com/eclypse-tms/umbra
// Noted: have a problem as clicked button will make the shadow keep added.

import UIKit

class ShadowOnButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    var shadowColor: UIColor = UIColor.darkGray
    
    var shadowOffset: CGPoint = CGPoint(x: 1, y: 1)
    
    var shadowOpacity: CGFloat = 0.5
    
    var shadowBlurRadius: CGFloat = 3.0
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setShadowLayer()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setShadowLayer()
    }
    
    private func setShadowLayer() {
        let shadowLayer = CAShapeLayer()
        
        //this button type does not use button configuration - check the main layer's corner radius
        shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
        
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.fillColor = backgroundColor?.cgColor
        shadowLayer.shadowColor = shadowColor.cgColor
        shadowLayer.shadowOffset = CGSize(width: shadowOffset.x, height: shadowOffset.y)
        shadowLayer.shadowOpacity = Float(shadowOpacity)
        shadowLayer.shadowRadius = shadowBlurRadius
        layer.insertSublayer(shadowLayer, at: 0)
    }
}

