//
//  WeatherViewModel.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 21/02/2024.
//

import Foundation
import CoreLocation

final class WeatherViewModel {
    private (set) var weatherModel: WeatherModel?
    private (set) var error: NetworkError?
    @Published private (set) var isLoading: Bool = false
    private (set) var hasError: Bool = false
    
    private let networkManager: WeatherNetworkManagerImpl!
    
    init(
        networkManagerImpl: WeatherNetworkManagerImpl = WeatherNetworkManager.shared
    ) {
        self.networkManager = networkManagerImpl
    }
    
    @MainActor
    func fetchWeather(
        for location: CLLocation?
    ) async {
        isLoading = true
        
        defer {
            isLoading = false
        }
        
        do {
            weatherModel = try await networkManager.request(
                session: .shared,
                endpoint: .detail(location: location),
                type: WeatherModel.self
            )
        }catch {
            hasError = true
            
            if let networkError = error as? NetworkError {
                self.error = networkError
            }else {
                self.error = .custom(error: error)
            }
        }
    }
}
