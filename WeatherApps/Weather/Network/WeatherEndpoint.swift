//
//  Endpoint.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 21/02/2024.
//

import Foundation
import CoreLocation

enum WeatherEndpoint {
    case detail(location: CLLocation?)
}

extension WeatherEndpoint {
    enum MethodType: Equatable {
        case GET
    }
}

extension WeatherEndpoint {
    var weather_api_key: String {
        return "d0c30802f983cb31a08e3f5c34b9a4a1"
    }
    
    var host: String {
        return "api.openweathermap.org"
    }
    
    var methodType: MethodType {
        switch self {
        case .detail(_):
            return .GET
        }
    }
    
    var path: String {
        switch self {
        case .detail(_):
            return "/data/2.5/onecall"
        }
    }
    
    var queryItems: [String: String] {
        switch self {
        case .detail(location: let location):
            guard let location else {
                return [
                    "lat": "33.44",
                    "lon": "-94.04",
                    "units": "metric",
                    "exclude": "minutely",
                    "appid": weather_api_key
                ]
            }
            
            return [
                "lat": "\(location.coordinate.latitude)",
                "lon": "\(location.coordinate.longitude)",
                "units": "metric",
                "exclude": "minutely",
                "appid": weather_api_key
            ]
        }
    }
}

extension WeatherEndpoint {
    var url: URL? {
        var urlComponent = URLComponents()
        urlComponent.scheme = "https"
        urlComponent.host = host
        urlComponent.path = path
        urlComponent.setQueryItems(with: queryItems)
        
        return urlComponent.url
    }
}
