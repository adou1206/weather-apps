//
//  SendRequest.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 20/02/2024.
//

import Foundation
import CoreLocation

protocol WeatherNetworkManagerImpl {
    func request<U: Decodable>(
        session: URLSession,
        endpoint: WeatherEndpoint,
        type: U.Type
    ) async throws -> U
}

final class WeatherNetworkManager: WeatherNetworkManagerImpl {
    static let shared = WeatherNetworkManager()
    
    private init() {}
    
    func request<U>(session: URLSession, endpoint: WeatherEndpoint, type: U.Type) async throws -> U where U : Decodable {
        guard let url = endpoint.url else {
            throw NetworkError.invalidURL
        }
//        print(url)
        let request = buildRequest(from: url, methodType: endpoint.methodType)
        
        let (data, response) = try await session.data(for: request)
//        print("decode response json: \(String(data: data, encoding: .utf8)!)")
        guard let response = response as? HTTPURLResponse,
              (200...300) ~= response.statusCode else {
            let statusCode = (response as! HTTPURLResponse).statusCode
            throw NetworkError.invalidStatusCode(statusCode: statusCode)
        }
        
        return try JSONDecoder().decode(U.self, from: data)
    }
}

private extension WeatherNetworkManager {
    func buildRequest(
        from url: URL,
        methodType: WeatherEndpoint.MethodType
    ) -> URLRequest {
        var request = URLRequest(url: url)
        
        switch methodType {
        case .GET:
            request.httpMethod = "GET"
        }
        
        return request
    }
}
