//
//  CustomNavigationController.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 09/04/2024.
//

import UIKit

// refer: https://stackoverflow.com/a/60917730

class CustomNavigationController: UINavigationController {
    var list_style: ListStyle? {
        didSet {
            guard let list_style = list_style else { return }
            
            setFont(list_style: list_style)
        }
    }
    
    private func setFont(list_style: ListStyle) {
        navigationBar.titleTextAttributes = [NSAttributedString.Key.font: list_style.navigationTitleFont()]
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: list_style.navigationBarButtonItemFont()], for: .normal)
    }
}
