//
//  LoadingTableViewCell.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 17/04/2024.
//

import UIKit

// refer: https://en.proft.me/2020/07/7/implementing-pagination-uitableviewuicollection/ (with early load data action)
class LoadingTableViewCell: UITableViewCell {
    static let identifier = "LoadingTableViewCell"
    
    lazy private var indicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        
        contentView.addSubview(indicator)
        
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        indicator.color = .black
        indicator.style = .medium
        indicator.hidesWhenStopped = true
        
        return indicator
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        backgroundColor = .clear
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func indicatorStartAnimating() {
        indicator.startAnimating()
    }
    
    public func indicatorStopAnimating() {
        indicator.stopAnimating()
    }
}
