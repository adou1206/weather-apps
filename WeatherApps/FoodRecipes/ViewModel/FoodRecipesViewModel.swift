//
//  FoodRecipesViewModel.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 26/03/2024.
//

import Foundation

final class FoodRecipesViewModel {
    private var foodRecipesModel: FoodRecipesModel?
    private (set) var hits: [Hit] = []
    private (set) var next: Next?
    private (set) var error: NetworkError?
    @Published private (set) var isLoading: Bool = false
    @Published private (set) var isLoadMore: Bool = false
    private (set) var hasError: Bool = false
    
    private let networkManager: FoodRecipesNetworkManagerImpl!
    
    init(
        networkManagerImpl: FoodRecipesNetworkManagerImpl = FoodRecipesNetworkManager.shared
    ) {
        self.networkManager = networkManagerImpl
    }
    
    @MainActor
    func fetchFoodRecipes(
        for q: String?,
        with cuisineType: [FoodRecipeCuisineTypes]? = nil
    ) async {
        isLoading = true
        
        defer {
            isLoading = false
        }
        
        do {
            foodRecipesModel = try await networkManager.request(
                session: .shared,
                endpoint: .detail(q: q, cuisineType: cuisineType),
                type: FoodRecipesModel.self
            )
            hits = foodRecipesModel?.hits ?? []
            next = foodRecipesModel?.links.next
            
        }catch {
            hasError = true

            if let networkError = error as? NetworkError {
                self.error = networkError
            }else {
                self.error = .custom(error: error)
            }
        }
    }
    
    @MainActor
    func fetchFoodRecipesNext(
        herf: String
    ) async {
        isLoadMore = true
        
        defer {
            isLoadMore = false
        }
        
        do {
            foodRecipesModel = try await networkManager.request(
                session: .shared,
                url_string: herf,
                methodType: .GET,
                type: FoodRecipesModel.self
            )
            hits.append(contentsOf: foodRecipesModel?.hits ?? [])
            next = foodRecipesModel?.links.next
            
        }catch {
            hasError = true

            if let networkError = error as? NetworkError {
                self.error = networkError
            }else {
                self.error = .custom(error: error)
            }
        }
    }
}
