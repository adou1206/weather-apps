//
//  FoodCategoryCollectionView.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 02/04/2024.
//

import UIKit

class FoodCategoryCollectionCell: UICollectionViewCell {
    static let identifier = "FoodCategoryCollectionCell"
    
    private var background_view = UIView()
    
    private var detail_stack_view = UIStackView()

    private var title_label = UILabel()

    private var category_image_view = UIImageView()
    
    override var isSelected: Bool {
        didSet {
            background_view.backgroundColor = isSelected ? ColorSets.food_recipe_text.color() : ColorSets.food_recipe_unselected.color()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        title_label.text = nil
        
        category_image_view.image = nil
    }
    
    func configure(cuisine_types: FoodRecipeCuisineTypes) {
        title_label.text = cuisine_types.text()
        
        category_image_view.image = cuisine_types.image()
    }
}

extension FoodCategoryCollectionCell {
    private func initView() {
        backgroundColor = .clear

        addSubView()
        viewUI()
        stackViewUI()
        labelViewUI()
        imageViewUI()
    }
    
    private func addSubView() {
        contentView.addSubview(background_view)
        
        background_view.addSubview(detail_stack_view)
        
        detail_stack_view.addArrangedSubview(category_image_view)
        detail_stack_view.addArrangedSubview(title_label)
    }
    
    private func viewUI() {
        background_view.translatesAutoresizingMaskIntoConstraints = false
        background_view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        background_view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        background_view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        background_view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        background_view.backgroundColor = ColorSets.food_recipe_unselected.color()
        background_view.layer.cornerRadius = 12
    }
    
    private func stackViewUI() {
        detail_stack_view.translatesAutoresizingMaskIntoConstraints = false
        detail_stack_view.topAnchor.constraint(equalTo: background_view.topAnchor).isActive = true
        detail_stack_view.bottomAnchor.constraint(equalTo: background_view.bottomAnchor).isActive = true
        detail_stack_view.leadingAnchor.constraint(equalTo: background_view.leadingAnchor).isActive = true
        detail_stack_view.trailingAnchor.constraint(equalTo: background_view.trailingAnchor).isActive = true
        detail_stack_view.axis = .vertical
        detail_stack_view.spacing = 0
        detail_stack_view.distribution = .fillEqually
//        detail_stack_view.alignment = .center
        detail_stack_view.contentLayoutMargins = .set.all(8).insets
    }
    
    private func labelViewUI() {
        title_label.font = .latoMedium(size: 12)
        title_label.textColor = ColorSets.white_text.color()
        title_label.textAlignment = .center
        title_label.numberOfLines = 0
        title_label.text = FoodRecipeCuisineTypes.south_east_asian.text()
    }
    
    private func imageViewUI() {
//        category_image_view.translatesAutoresizingMaskIntoConstraints = false
//        category_image_view.heightAnchor.constraint(equalTo: detail_stack_view.heightAnchor, multiplier: 0.7).isActive = true
//        category_image_view.widthAnchor.constraint(equalTo: category_image_view.heightAnchor).isActive = true
        category_image_view.contentMode = .scaleAspectFit
        category_image_view.clipsToBounds = true
        category_image_view.image = UIImage(named: "test")
    }
}
