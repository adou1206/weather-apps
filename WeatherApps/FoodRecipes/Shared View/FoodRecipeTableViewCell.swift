//
//  FoodRecipeTableViewCell.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 02/04/2024.
//

import UIKit

class FoodRecipeTableViewCell: UITableViewCell {
    static let identifier = "FoodRecipeTableViewCell"
    
    var background_view_color: ColorSets? {
        didSet {
            guard let background_view_color else { return }
            
            background_view.backgroundColor = background_view_color.color()
        }
    }
    
    private let public_method = PublicMethod.shared
    
    private var background_view = UIView()
    
    private var detail_stack_view = UIStackView()
    private var text_stack_view = UIStackView()
    private var time_stack_view = UIStackView()
    
    private var title_label = UILabel()
    private var time_label = UILabel()
    private var diet_health_label = UILabel()
    private var source_label = UILabel()
    
    private var dish_image_view = UIImageView()
    private var time_icon_image_view = UIImageView()
    
    private var image_request: Cancellable?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        dish_image_view.image = nil
        
        title_label.text = nil
        time_label.text = nil
        diet_health_label.text = nil
        source_label.text = nil
        
        if let image_request = image_request {
            image_request.cancel()
        }
    }
    
    func configure(view_model: Recipe) {
        image_request = dish_image_view.loadFrom(URLAddress: view_model.image)
        
        title_label.text = view_model.label
        time_label.text = public_method.generateTimeDisplay(minutes: view_model.totalTime)
        diet_health_label.text = (view_model.dietLabels.joined(separator: " • ") + " • " + view_model.healthLabels.joined(separator: " • ")).replacingOccurrences(of: "-", with: " ")
        source_label.text = view_model.source
    }
}

extension FoodRecipeTableViewCell {
    private func initView() {
        backgroundColor = .clear
        selectionStyle = .none
        
        addSubView()
        viewUI()
        stackViewUI()
        labelViewUI()
        imageViewUI()
    }
    
    private func addSubView() {
        contentView.addSubview(background_view)
        
        background_view.addSubview(detail_stack_view)
        
        detail_stack_view.addArrangedSubview(dish_image_view)
        detail_stack_view.addArrangedSubview(text_stack_view)
        
        text_stack_view.addArrangedSubview(title_label)
        text_stack_view.addArrangedSubview(time_stack_view)
        text_stack_view.addArrangedSubview(diet_health_label)
        text_stack_view.addArrangedSubview(source_label)
        
        time_stack_view.addArrangedSubview(time_icon_image_view)
        time_stack_view.addArrangedSubview(time_label)
    }
    
    private func viewUI() {
        background_view.translatesAutoresizingMaskIntoConstraints = false
        background_view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        background_view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20).isActive = true
        background_view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        background_view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        background_view.layer.cornerRadius = 12
    }
    
    private func stackViewUI() {
        detail_stack_view.translatesAutoresizingMaskIntoConstraints = false
        detail_stack_view.topAnchor.constraint(equalTo: background_view.topAnchor).isActive = true
        detail_stack_view.bottomAnchor.constraint(equalTo: background_view.bottomAnchor).isActive = true
        detail_stack_view.leadingAnchor.constraint(equalTo: background_view.leadingAnchor).isActive = true
        detail_stack_view.trailingAnchor.constraint(equalTo: background_view.trailingAnchor).isActive = true
        detail_stack_view.axis = .horizontal
        detail_stack_view.spacing = 8
        detail_stack_view.distribution = .fill
        detail_stack_view.alignment = .center
        detail_stack_view.contentLayoutMargins = .set.top(12).bottom(12).left(6).right(6).insets
        
        text_stack_view.axis = .vertical
        text_stack_view.spacing = 6
        text_stack_view.distribution = .fill
        
        time_stack_view.axis = .horizontal
        time_stack_view.spacing = 6
        time_stack_view.distribution = .fill
        time_stack_view.backgroundColor = .clear
    }
    
    private func labelViewUI() {
        title_label.font = .latoSemiBold(size: 15)
        title_label.textColor = ColorSets.food_recipe_text.color()
        title_label.textAlignment = .left
        title_label.numberOfLines = 0
        
        time_label.font = .latoMedium(size: 13)
        time_label.textColor = ColorSets.food_recipe_unselected.color()
        time_label.textAlignment = .left
        time_label.numberOfLines = 0
        
        diet_health_label.font = .latoRegular(size: 14)
        diet_health_label.textColor = ColorSets.food_recipe_text.color()
        diet_health_label.textAlignment = .left
        diet_health_label.numberOfLines = 5
        
        source_label.font = .latoMedium(size: 13)
        source_label.textColor = ColorSets.food_recipe_text.color()
        source_label.textAlignment = .left
        source_label.numberOfLines = 1
    }
    
    private func imageViewUI() {
        dish_image_view.translatesAutoresizingMaskIntoConstraints = false
        dish_image_view.widthAnchor.constraint(equalTo: detail_stack_view.widthAnchor, multiplier: 0.4).isActive = true
        dish_image_view.heightAnchor.constraint(equalTo: dish_image_view.widthAnchor).isActive = true
        dish_image_view.contentMode = .scaleAspectFit
        dish_image_view.clipsToBounds = true
        dish_image_view.layer.cornerRadius = 12

        time_icon_image_view.translatesAutoresizingMaskIntoConstraints = false
        time_icon_image_view.widthAnchor.constraint(equalTo: time_stack_view.widthAnchor, multiplier: 0.12).isActive = true
        time_icon_image_view.contentMode = .scaleAspectFit
        time_icon_image_view.clipsToBounds = true
        time_icon_image_view.image = UIImage(
            systemName: "clock"
        )?.withTintColor(
                ColorSets.food_recipe_unselected.color()!,
                renderingMode: .alwaysOriginal
        )
    }
}
