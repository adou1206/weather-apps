//
//  FoodRecipeCollectionCell.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 27/03/2024.
//

import UIKit

// Noted: this is an unused class.
class FoodRecipeCollectionCell: UICollectionViewCell {
    static let identifier = "FoodRecipeCollectionCell"
    
    private var background_view = UIView()
    
    private var detail_stack_view = UIStackView()

    private var title_label = UILabel()
    private var source_label = UILabel()

    private var dish_image_view = UIImageView()
    
    private var image_request: Cancellable?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        title_label.text = nil
        source_label.text = nil
        
        dish_image_view.image = nil
        
        if let image_request = image_request {
            image_request.cancel()
        }
    }
    
    func configure(view_model: Recipe) {
        title_label.text = view_model.label
        source_label.text = view_model.source
        
        image_request = dish_image_view.loadFrom(URLAddress: view_model.image)
    }
}

extension FoodRecipeCollectionCell {
    private func initView() {
        layer.cornerRadius = 12
        
        addSubView()
        viewUI()
        stackViewUI()
        labelViewUI()
        imageViewUI()
    }
    
    private func addSubView() {
        contentView.addSubview(background_view)
        
        background_view.addSubview(detail_stack_view)
        background_view.addSubview(dish_image_view)
        
        detail_stack_view.addArrangedSubview(title_label)
        detail_stack_view.addArrangedSubview(source_label)
    }
     
    private func viewUI() {
        background_view.translatesAutoresizingMaskIntoConstraints = false
        background_view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        background_view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        background_view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        background_view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        background_view.backgroundColor = .clear
    }
    
    private func stackViewUI() {
        detail_stack_view.translatesAutoresizingMaskIntoConstraints = false
        detail_stack_view.topAnchor.constraint(equalTo: dish_image_view.bottomAnchor).isActive = true
        detail_stack_view.bottomAnchor.constraint(equalTo: background_view.bottomAnchor).isActive = true
        detail_stack_view.leadingAnchor.constraint(equalTo: background_view.leadingAnchor).isActive = true
        detail_stack_view.trailingAnchor.constraint(equalTo: background_view.trailingAnchor).isActive = true
        detail_stack_view.contentLayoutMargins = .set.all(6).insets
        detail_stack_view.axis = .vertical
        detail_stack_view.distribution = .fill
        detail_stack_view.alignment = .fill
        detail_stack_view.spacing = 8
        detail_stack_view.backgroundColor = .clear
    }
    
    private func labelViewUI() {
        title_label.font = .latoSemiBold(size: 16)
        title_label.textColor = ColorSets.black_text.color()
        title_label.textAlignment = .left
        title_label.numberOfLines = 2
        
        source_label.font = .latoMedium(size: 14)
        source_label.textColor = ColorSets.black_text.color()
        source_label.textAlignment = .left
        source_label.numberOfLines = 2
    }
    
    private func imageViewUI() {
        dish_image_view.translatesAutoresizingMaskIntoConstraints = false
        dish_image_view.topAnchor.constraint(equalTo: background_view.topAnchor).isActive = true
        dish_image_view.leadingAnchor.constraint(equalTo: background_view.leadingAnchor).isActive = true
        dish_image_view.trailingAnchor.constraint(equalTo: background_view.trailingAnchor).isActive = true
        dish_image_view.heightAnchor.constraint(equalToConstant: 220).isActive = true
        dish_image_view.backgroundColor = .systemRed
        dish_image_view.contentMode = .scaleToFill
        dish_image_view.clipsToBounds = true
    }
}
