//
//  FoodRecipeCollectionReusableView.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 01/04/2024.
//

import UIKit

protocol FoodRecipeCollectionSearchBtnTapDelegate: AnyObject {
    /// food recipe collection search button clicked action
    func didTapSearchBtn()
}

class FoodRecipeCollectionReusableView: UICollectionReusableView {
    static let identifier = "FoodRecipeCollectionReusableView"
    
    private weak var delegate: FoodRecipeCollectionSearchBtnTapDelegate?
    
    private var detail_stack_view = UIStackView()
    
    private var title_label = UILabel()
    
    private var search_btn = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(title: String, tap_delegate: FoodRecipeCollectionSearchBtnTapDelegate) {
        title_label.text = title
        
        delegate = tap_delegate
    }
}

extension FoodRecipeCollectionReusableView {
    private func initView() {
        addSubView()
        stackViewUI()
        labelViewUI()
        buttonViewUI()
    }
    
    private func addSubView() {
        addSubview(detail_stack_view)
        
        detail_stack_view.addArrangedSubview(title_label)
        detail_stack_view.addArrangedSubview(search_btn)
    }
    
    private func stackViewUI() {
        detail_stack_view.translatesAutoresizingMaskIntoConstraints = false
        detail_stack_view.topAnchor.constraint(equalTo: topAnchor).isActive = true
        detail_stack_view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        detail_stack_view.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        detail_stack_view.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        detail_stack_view.contentLayoutMargins = .set.bottom(12).right(12).insets
        detail_stack_view.axis = .horizontal
        detail_stack_view.spacing = 8
        detail_stack_view.alignment = .center
        detail_stack_view.distribution = .fill
    }
    
    private func labelViewUI() {
        title_label.font = .latoMedium(size: 22)
        title_label.numberOfLines = 0
        title_label.textAlignment = .left
        title_label.textColor = ColorSets.food_recipe_text.color()
    }
    
    private func buttonViewUI() {
        search_btn.translatesAutoresizingMaskIntoConstraints = false
        search_btn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        search_btn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        search_btn.contentVerticalAlignment = .fill
        search_btn.contentHorizontalAlignment = .fill
        search_btn.backgroundColor = .clear
        search_btn.setImage(
            UIImage(
                systemName: "magnifyingglass.circle"
            )?.withTintColor(
                .white,
                renderingMode: .alwaysOriginal
            ),
            for: .normal
        )
        search_btn.addTarget(self, action: #selector(searchBtnAction), for: .touchUpInside)
    }
    
    @objc private func searchBtnAction() {
        guard let tap_delegate = delegate else {
            NSLog("Protocol not found")
            return
        }
        
        tap_delegate.didTapSearchBtn()
    }
}
