//
//  IngredientsTableViewCell.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 04/04/2024.
//

import UIKit

class IngredientsTableViewCell: UITableViewCell {
    static let identifier = "IngredientsTableViewCell"
    
    private var background_view = UIView()
    
    private var ingredient_stack_view = UIStackView()
    
    private var name_label = UILabel()
    private var quantity_label = UILabel()
    
    private var ingredient_image_view = UIImageView()
    
    private var image_request: Cancellable?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        ingredient_image_view.image = nil
        
        name_label.text = nil
        quantity_label.text = nil
        
        if let image_request = image_request {
            image_request.cancel()
        }
    }
    
    func configure(view_model: Ingredient) {
        name_label.text = view_model.food
        quantity_label.text = view_model.text
        
        guard let image_url = view_model.image else { return }
        
        image_request = ingredient_image_view.loadFrom(URLAddress: image_url)
    }
}

extension IngredientsTableViewCell {
    private func initView() {
        selectionStyle = .none
        backgroundColor = ColorSets.food_recipe_unselected.color()
        
        addSubView()
        viewUI()
        stackViewUI()
        imageViewUI()
        labelViewUI()
    }
    
    private func addSubView() {
        contentView.addSubview(background_view)
        
        background_view.addSubview(ingredient_stack_view)
        
        ingredient_stack_view.addArrangedSubview(ingredient_image_view)
        ingredient_stack_view.addArrangedSubview(name_label)
        ingredient_stack_view.addArrangedSubview(quantity_label)
    }
    
    private func viewUI() {
        background_view.translatesAutoresizingMaskIntoConstraints = false
        background_view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        background_view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        background_view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        background_view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        background_view.backgroundColor = .clear
    }
    
    private func stackViewUI() {
        ingredient_stack_view.translatesAutoresizingMaskIntoConstraints = false
        ingredient_stack_view.topAnchor.constraint(equalTo: background_view.topAnchor).isActive = true
        ingredient_stack_view.bottomAnchor.constraint(equalTo: background_view.bottomAnchor).isActive = true
        ingredient_stack_view.leadingAnchor.constraint(equalTo: background_view.leadingAnchor).isActive = true
        ingredient_stack_view.trailingAnchor.constraint(equalTo: background_view.trailingAnchor).isActive = true
        ingredient_stack_view.axis = .horizontal
        ingredient_stack_view.spacing = 12
        ingredient_stack_view.distribution = .fill
        ingredient_stack_view.alignment = .center
        ingredient_stack_view.contentLayoutMargins = .set.top(16).bottom(16).left(12).right(12).insets
        ingredient_stack_view.backgroundColor = .clear
    }
    
    private func labelViewUI() {
        name_label.translatesAutoresizingMaskIntoConstraints = false
        name_label.widthAnchor.constraint(equalTo: ingredient_stack_view.widthAnchor, multiplier: 0.35).isActive = true
        name_label.font = .latoMedium(size: 15)
        name_label.textColor = ColorSets.white_text.color()
        name_label.textAlignment = .left
        name_label.numberOfLines = 0
        
        quantity_label.font = .latoRegular(size: 15)
        quantity_label.textColor = ColorSets.white_text.color()?.withAlphaComponent(0.8)
        quantity_label.textAlignment = .left
        quantity_label.numberOfLines = 0
    }
    
    private func imageViewUI() {
        ingredient_image_view.translatesAutoresizingMaskIntoConstraints = false
        ingredient_image_view.widthAnchor.constraint(equalTo: ingredient_stack_view.widthAnchor, multiplier: 0.2).isActive = true
        ingredient_image_view.heightAnchor.constraint(equalTo: ingredient_image_view.widthAnchor).isActive = true
        ingredient_image_view.contentMode = .scaleAspectFit
        ingredient_image_view.clipsToBounds = true
        ingredient_image_view.layer.cornerRadius = 12
    }
}
