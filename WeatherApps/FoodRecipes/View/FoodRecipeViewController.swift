//
//  FoodRecipeViewController.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 26/03/2024.
//

import UIKit
import Combine

/*
 
 MARK: Maybe can try add Cached Image action
 MARK: Maybe can check "nw_connection_add_timestamp_locked_on_nw_queue [C54] Hit maximum timestamp count, will start dropping events" why have this warning
 
 */

class FoodRecipeViewController: UIViewController {
    private var vm: FoodRecipesViewModel
    
    private let cuisine_category_list: [FoodRecipeCuisineTypes] = FoodRecipeCuisineTypes.allCases
    
    private var selected_cuisine_category_list: [FoodRecipeCuisineTypes] = [.american]
    
    private lazy var collection_view: UICollectionView = {
        return UICollectionView(frame: .zero, collectionViewLayout: createLayout())
    }()
    
    // refer: https://stackoverflow.com/a/51710722
    private var table_view = UITableView(frame: .zero, style: .grouped)
    
    private var search_bar = UISearchBar()
    
    private var recipe_list_stack_view = UIStackView()
    
    private var recipe_list_title_label = UILabel()
    
    private var recipe_list_scroll_to_top_btn = UIButton()
    
    private var isLoading: Bool = false {
        didSet {
            isLoading ? showSpinner() : removeSpinner()
        }
    }
    
    private var isLoadMore: Bool = false
    
    private var bindings = Set<AnyCancellable>()
    
    init() {
        self.vm = FoodRecipesViewModel()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if let nvc = navigationController as? CustomNavigationController {
            nvc.list_style = .food_recipe_style
        }
        
        initView()
        
        fetchDate(serach_text: "", cuisine_type: [.american])
    }
}

extension FoodRecipeViewController {
    private func initView() {
        addPreviousNextableDoneButtonOnKeyboard(objects: [
            ToolbarCustomButtonViewModel(obj: search_bar.searchTextField, doneButtonTitle: "Close")
        ])
        
        navigationItem.title = ListStyle.food_recipe_style.titleText()
        
        view.layer.addSublayer(ListStyle.food_recipe_style.gradientLayer(in: view.frame))
        
        addSubView()
        searchBarUI()
        collectionViewUI()
        stackViewUI()
        labelViewUI()
        buttonViewUI()
        tableViewUI()
        vm.$isLoading.assign(to: \.isLoading, on: self).store(in: &bindings)
        vm.$isLoadMore.assign(to: \.isLoadMore, on: self).store(in: &bindings)
    }
    
    private func addSubView() {
        view.addSubview(search_bar)
        view.addSubview(collection_view)
        view.addSubview(recipe_list_stack_view)
        view.addSubview(table_view)
        
        recipe_list_stack_view.addArrangedSubview(recipe_list_title_label)
        recipe_list_stack_view.addArrangedSubview(recipe_list_scroll_to_top_btn)
    }
    
    private func searchBarUI() {
        search_bar.translatesAutoresizingMaskIntoConstraints = false
        search_bar.topAnchor.constraint(equalTo: view.safeTopAnchor).isActive = true
        search_bar.leadingAnchor.constraint(equalTo: view.safeLeadingAnchor, constant: 15).isActive = true
        search_bar.trailingAnchor.constraint(equalTo: view.safeTrailingAnchor, constant: -15).isActive = true
        search_bar.searchBarStyle = .minimal
        search_bar.returnKeyType = .search
        search_bar.backgroundColor = .clear
        search_bar.delegate = self
        
        // refer: https://stackoverflow.com/a/66744373
        let image = UIImage(
            systemName: "magnifyingglass"
        )?.withTintColor(
            .white,
            renderingMode: .alwaysOriginal
        )
        UISearchBar.appearance().setImage(image, for: .search, state: .normal)
        
        search_bar.searchTextField.font = .latoRegular(size: 14)
        search_bar.searchTextField.textColor = .white
        search_bar.searchTextField.backgroundColor = ColorSets.food_recipe_unselected.color()?.withAlphaComponent(0.5)
        
        // refer: https://stackoverflow.com/a/66198841
        search_bar.searchTextField.attributedPlaceholder = NSAttributedString(
            string: "Search for recipes",
            attributes: [
                .foregroundColor: UIColor.white
            ]
        )
    }
    
    private func collectionViewUI() {
        collection_view.translatesAutoresizingMaskIntoConstraints = false
        collection_view.topAnchor.constraint(equalTo: search_bar.bottomAnchor).isActive = true
        collection_view.leadingAnchor.constraint(equalTo: search_bar.leadingAnchor, constant: 8).isActive = true
        collection_view.trailingAnchor.constraint(equalTo: search_bar.trailingAnchor, constant: -8).isActive = true
        collection_view.heightAnchor.constraint(equalToConstant: 160).isActive = true
        collection_view.showsVerticalScrollIndicator = false
        collection_view.showsHorizontalScrollIndicator = false
        collection_view.allowsMultipleSelection = true
        collection_view.backgroundColor = .clear
        collection_view.register(
            FoodCategoryCollectionCell.self,
            forCellWithReuseIdentifier: FoodCategoryCollectionCell.identifier
        )
        collection_view.register(FoodRecipeCollectionReusableView.self,
                                 forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                 withReuseIdentifier: FoodRecipeCollectionReusableView.identifier
        )
        collection_view.dataSource = self
        collection_view.delegate = self
    }
    
    private func tableViewUI() {
        table_view.translatesAutoresizingMaskIntoConstraints = false
        table_view.topAnchor.constraint(equalTo: recipe_list_stack_view.bottomAnchor).isActive = true
        table_view.bottomAnchor.constraint(equalTo: view.safeBottomAnchor).isActive = true
        table_view.leadingAnchor.constraint(equalTo: recipe_list_stack_view.leadingAnchor).isActive = true
        table_view.trailingAnchor.constraint(equalTo: recipe_list_stack_view.trailingAnchor).isActive = true
        table_view.separatorStyle = .none
        table_view.register(FoodRecipeTableViewCell.self, forCellReuseIdentifier: FoodRecipeTableViewCell.identifier)
        table_view.register(LoadingTableViewCell.self, forCellReuseIdentifier: LoadingTableViewCell.identifier)
        table_view.showsHorizontalScrollIndicator = false
        table_view.backgroundColor = .clear
        table_view.dataSource = self
        table_view.delegate = self
    }
    
    private func stackViewUI() {
        recipe_list_stack_view.translatesAutoresizingMaskIntoConstraints = false
        recipe_list_stack_view.topAnchor.constraint(equalTo: collection_view.bottomAnchor).isActive = true
        recipe_list_stack_view.leadingAnchor.constraint(equalTo: collection_view.leadingAnchor).isActive = true
        recipe_list_stack_view.trailingAnchor.constraint(equalTo: collection_view.trailingAnchor).isActive = true
        recipe_list_stack_view.contentLayoutMargins = .set.bottom(12).right(12).insets
        recipe_list_stack_view.axis = .horizontal
        recipe_list_stack_view.spacing = 8
        recipe_list_stack_view.alignment = .center
        recipe_list_stack_view.distribution = .fill
    }
    
    private func labelViewUI() {
        recipe_list_title_label.textAlignment = .left
        recipe_list_title_label.textColor = ColorSets.food_recipe_text.color()
        recipe_list_title_label.font = .latoRegular(size: 22)
        recipe_list_title_label.numberOfLines = 0
        recipe_list_title_label.text = "Recipe"
    }
    
    private func buttonViewUI() {
        recipe_list_scroll_to_top_btn.translatesAutoresizingMaskIntoConstraints = false
        recipe_list_scroll_to_top_btn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        recipe_list_scroll_to_top_btn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        recipe_list_scroll_to_top_btn.contentVerticalAlignment = .fill
        recipe_list_scroll_to_top_btn.contentHorizontalAlignment = .fill
        recipe_list_scroll_to_top_btn.backgroundColor = .clear
        recipe_list_scroll_to_top_btn.setImage(
            UIImage(
                systemName: "arrow.up.circle"
            )?.withTintColor(
                .white,
                renderingMode: .alwaysOriginal
            ),
            for: .normal
        )
        recipe_list_scroll_to_top_btn.addTarget(self, action: #selector(scrollToTop), for: .touchUpInside)
    }
    
    private func createLayout() -> UICollectionViewCompositionalLayout {
        return UICollectionViewCompositionalLayout { [weak self] (sectionNumber, env) in
            guard let self = self else { return nil }
            
            let item = CompositionalLayout.createItem(width: .fractionalWidth(1), height: .fractionalHeight(1))
            
            let group = CompositionalLayout.createGroup(aligment: .horizontal, width: .fractionalWidth(0.23), height: .fractionalHeight(0.67), items: [item])
            
            let section = NSCollectionLayoutSection(group: group)
            section.interGroupSpacing = 10
            section.contentInsets = .init(top: 0, leading: 0, bottom: 0, trailing: 0)
            section.boundarySupplementaryItems = [self.supplementaryHeaderItem()]
            section.supplementariesFollowContentInsets = false
            section.orthogonalScrollingBehavior = .continuousGroupLeadingBoundary
            
            return section
        }
    }
    
    private func supplementaryHeaderItem() -> NSCollectionLayoutBoundarySupplementaryItem {
        .init(
            layoutSize: .init(
                widthDimension: .fractionalWidth(1),
                heightDimension: .estimated(50)
            ),
            elementKind: UICollectionView.elementKindSectionHeader,
            alignment: .top
        )
    }
    
    private func checkSearchAndCuisineEmpty() -> Bool {
        if (search_bar.text ?? "").isEmpty, selected_cuisine_category_list.count < 1 {
            return false
        }
        
        return true
    }
    
    private func displayEmptyDataSearchMessage() {
        alertWithTitle("Notice", message: "Please select at least one Cuisine Category or Enter Search Text.")
    }
    
    // refer: https://programmingwithswift.com/swift-scroll-to-top-of-uitableview/
    @objc private func scrollToTop() {
        guard vm.hits.count > 0 else { return }
        
        // 1
        let top_row = IndexPath(
            row: 0,
            section: 0
        )

        // 2
        table_view.scrollToRow(
            at: top_row,
            at: .top,
            animated: true
        )
    }
    
    private func fetchDate(
        serach_text: String,
        cuisine_type: [FoodRecipeCuisineTypes]
    ) {
        Task {
            defer {
                if vm.hasError {
                    alertWithTitle("Notice", message: vm.error?.errorDescription ?? "")
                    
                }else {
                    table_view.reloadData()
                    
                    recipe_list_scroll_to_top_btn.sendActions(for: .touchUpInside)
                }
            }
            
            await vm.fetchFoodRecipes(for: serach_text, with: cuisine_type)
        }
    }
    
    private func loadMoreData() {
        if !isLoading {
            guard let next = vm.next, !next.href.isEmpty else { return }
            
            Task {
                defer {
                    if vm.hasError {
                        alertWithTitle("Notice", message: vm.error?.errorDescription ?? "")
                        
                    }else {
                        table_view.reloadData()
                    }
                }
                
                await vm.fetchFoodRecipesNext(herf: next.href)
            }
        }
    }
}

// scrollViewDidScroll function part extension
extension FoodRecipeViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.bounces = (scrollView.contentOffset.y > 100)
        
        // refer: https://en.proft.me/2020/07/7/implementing-pagination-uitableviewuicollection/ (with early load data action)
        if scrollView == table_view {
            let offsetY = scrollView.contentOffset.y
            let contentHeight = scrollView.contentSize.height
            
            if (offsetY > contentHeight - scrollView.frame.height * 2) && !isLoadMore {
                loadMoreData()
            }
        }
    }
}

extension FoodRecipeViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        
        guard checkSearchAndCuisineEmpty() else {
            displayEmptyDataSearchMessage()
            
            return
        }
        
        fetchDate(
            serach_text: searchBar.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
            cuisine_type: selected_cuisine_category_list
        )
    }
}

extension FoodRecipeViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return vm.hits.count
            
        } else if section == 1 {
            // loading cell
            return 1
            
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: FoodRecipeTableViewCell.identifier, for: indexPath) as! FoodRecipeTableViewCell
            cell.background_view_color = indexPath.row % 2 == 0 ? .food_recipe_detail_background : .food_recipe_detail_background2
            cell.configure(view_model: vm.hits[indexPath.row].recipe)
            
            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadingTableViewCell.identifier, for: indexPath) as! LoadingTableViewCell
            
            if isLoading {
                cell.indicatorStartAnimating()
                
            }else {
                cell.indicatorStopAnimating()
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let nvc = navigationController {
            let food_recipe_details_vc = FoodRecipeDetailsViewController(view_model: vm.hits[indexPath.row].recipe)
            
            nvc.pushViewController(food_recipe_details_vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
            
        } else {
            return 55 // loading cell
        }
    }
}

extension FoodRecipeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cuisine_category_list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let header_view = collectionView.dequeueReusableSupplementaryView(
                ofKind: UICollectionView.elementKindSectionHeader,
                withReuseIdentifier: FoodRecipeCollectionReusableView.identifier,
                for: indexPath
            ) as! FoodRecipeCollectionReusableView
            
            header_view.configure(title: "Cuisine Category", tap_delegate: self)
            
            return header_view
            
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // refer: https://stackoverflow.com/a/45610881
        if indexPath.row == 0 {
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .left)
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FoodCategoryCollectionCell.identifier, for: indexPath) as! FoodCategoryCollectionCell
        cell.configure(cuisine_types: cuisine_category_list[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selected_cuisine_category_list.append(cuisine_category_list[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        selected_cuisine_category_list.remove(object: cuisine_category_list[indexPath.row])
    }
}

extension FoodRecipeViewController: FoodRecipeCollectionSearchBtnTapDelegate {
    func didTapSearchBtn() {
        guard selected_cuisine_category_list.count > 0 else {
            displayEmptyDataSearchMessage()
            
            return
        }
        
        fetchDate(serach_text: search_bar.text ?? "", cuisine_type: selected_cuisine_category_list)
    }
}
