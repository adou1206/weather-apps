//
//  FoodRecipeDetailsViewController.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 03/04/2024.
//

import UIKit

class FoodRecipeDetailsViewController: UIViewController {
    private let public_method = PublicMethod.shared
    
    private var detail_view = UIView()
    
    private var background_stack_view = UIStackView()
    private var detail_stack_view = UIStackView()
    private var title_stack_view = UIStackView()
    private var time_stack_view = UIStackView()
    
    private var ingredients_table_view = UITableView()
    
    private var title_label = UILabel()
    private var time_label = UILabel()
    private var diet_health_label = UILabel()
    private var source_label = UILabel()
    private var ingredients_label = UILabel()
    
    private var dish_image_view = UIImageView()
    private var time_icon_image_view = UIImageView()

    private var view_model: Recipe!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    convenience init(view_model: Recipe) {
        self.init()
        self.view_model = view_model
    }
}

extension FoodRecipeDetailsViewController {
    private func initView() {
        navigationItem.title = view_model.label
        
        let gradient_layer = CAGradientLayer.gradientLayer(for: .food_recipe_style, in: view.frame)
        view.layer.addSublayer(gradient_layer)
        
        addSubView()
        viewUI()
        stackViewUI()
        tableViewUI()
        labelViewUI()
        imageViewUI()
        updateDisplay()
    }
    
    private func addSubView() {
        view.addSubview(background_stack_view)
        
        background_stack_view.addArrangedSubview(dish_image_view)
        background_stack_view.addArrangedSubview(detail_view)
        
        detail_view.addSubview(detail_stack_view)
        
        detail_stack_view.addArrangedSubview(title_stack_view)
        detail_stack_view.addArrangedSubview(diet_health_label)
        detail_stack_view.addArrangedSubview(source_label)
        detail_stack_view.addArrangedSubview(ingredients_label)
        detail_stack_view.addArrangedSubview(ingredients_table_view)
        
        title_stack_view.addArrangedSubview(title_label)
        title_stack_view.addArrangedSubview(time_stack_view)
        
        time_stack_view.addArrangedSubview(time_icon_image_view)
        time_stack_view.addArrangedSubview(time_label)
    }
    
    private func viewUI() {
        detail_view.layer.cornerRadius = 45
        detail_view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        detail_view.backgroundColor = ColorSets.food_recipe_text.color()
    }
    
    private func stackViewUI() {
        background_stack_view.translatesAutoresizingMaskIntoConstraints = false
        background_stack_view.topAnchor.constraint(equalTo: view.safeTopAnchor).isActive = true
        background_stack_view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        background_stack_view.leadingAnchor.constraint(equalTo: view.safeLeadingAnchor).isActive = true
        background_stack_view.trailingAnchor.constraint(equalTo: view.safeTrailingAnchor).isActive = true
        background_stack_view.axis = .vertical
        background_stack_view.spacing = 10
        background_stack_view.distribution = .fill
        background_stack_view.backgroundColor = .clear
        
        detail_stack_view.translatesAutoresizingMaskIntoConstraints = false
        detail_stack_view.topAnchor.constraint(equalTo: detail_view.topAnchor).isActive = true
        detail_stack_view.bottomAnchor.constraint(equalTo: detail_view.bottomAnchor).isActive = true
        detail_stack_view.leadingAnchor.constraint(equalTo: detail_view.leadingAnchor).isActive = true
        detail_stack_view.trailingAnchor.constraint(equalTo: detail_view.trailingAnchor).isActive = true
        detail_stack_view.axis = .vertical
        detail_stack_view.spacing = 10
        detail_stack_view.distribution = .fill
        detail_stack_view.contentLayoutMargins = .set.top(22).bottom(15).left(18).right(18).insets
        detail_stack_view.backgroundColor = .clear
        
        title_stack_view.axis = .horizontal
        title_stack_view.spacing = 12
        title_stack_view.distribution = .fill
        
        time_stack_view.translatesAutoresizingMaskIntoConstraints = false
        time_stack_view.widthAnchor.constraint(equalTo: title_stack_view.widthAnchor, multiplier: 0.3).isActive = true
        time_stack_view.axis = .horizontal
        time_stack_view.spacing = 6
        time_stack_view.distribution = .fill
        time_stack_view.backgroundColor = .clear
        time_stack_view.alignment = .center
    }
    
    private func tableViewUI() {
        ingredients_table_view.register(IngredientsTableViewCell.self, forCellReuseIdentifier: IngredientsTableViewCell.identifier)
        ingredients_table_view.separatorStyle = .singleLine
        ingredients_table_view.separatorInset = .init(top: 0, left: 30, bottom: 0, right: 30)
        ingredients_table_view.layer.cornerRadius = 12
        ingredients_table_view.showsVerticalScrollIndicator = false
        ingredients_table_view.showsHorizontalScrollIndicator = false
        ingredients_table_view.dataSource = self
        ingredients_table_view.delegate = self
        ingredients_table_view.backgroundColor = .clear
    }
    
    private func labelViewUI() {
        diet_health_label.font = .latoRegular(size: 15)
        diet_health_label.textColor = ColorSets.food_recipe_unselected.color()
        diet_health_label.numberOfLines = 0
        diet_health_label.textAlignment = .left
        
        source_label.font = .latoMedium(size: 13)
        source_label.textColor = ColorSets.food_recipe_unselected.color()
        source_label.textAlignment = .left
        source_label.numberOfLines = 0
        
        ingredients_label.textAlignment = .left
        ingredients_label.textColor = ColorSets.food_recipe_selected.color()
        ingredients_label.font = .latoRegular(size: 18)
        ingredients_label.numberOfLines = 0
        ingredients_label.text = "Ingredients"
        
        title_label.font = .latoSemiBold(size: 19)
        title_label.textColor = ColorSets.white_text.color()
        title_label.textAlignment = .left
        title_label.numberOfLines = 0
        
        time_label.font = .latoMedium(size: 13)
        time_label.textColor = ColorSets.food_recipe_button.color()
        time_label.textAlignment = .left
        time_label.numberOfLines = 0
    }
    
    private func imageViewUI() {
        dish_image_view.translatesAutoresizingMaskIntoConstraints = false
        dish_image_view.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.28).isActive = true
        dish_image_view.contentMode = .scaleAspectFit
        dish_image_view.clipsToBounds = true
        
        time_icon_image_view.translatesAutoresizingMaskIntoConstraints = false
        time_icon_image_view.widthAnchor.constraint(equalTo: time_stack_view.widthAnchor, multiplier: 0.2).isActive = true
        time_icon_image_view.contentMode = .scaleAspectFit
        time_icon_image_view.clipsToBounds = true
        time_icon_image_view.image = UIImage(
            systemName: "clock"
        )?.withTintColor(
                ColorSets.food_recipe_button.color()!,
                renderingMode: .alwaysOriginal
        )
    }
    
    private func updateDisplay() {
        guard let view_model = view_model else { return }
        
        _ = dish_image_view.loadFrom(URLAddress: view_model.image, image_corner_radius: 20)
        
        title_label.text = view_model.label
        time_label.text = public_method.generateTimeDisplay(minutes: view_model.totalTime)
        diet_health_label.text = (view_model.dietLabels.joined(separator: " • ") + " • " + view_model.healthLabels.joined(separator: " • ")).replacingOccurrences(of: "-", with: " ")
        source_label.text = view_model.source

        ingredients_table_view.reloadData()
    }
}

extension FoodRecipeDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return view_model.ingredients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: IngredientsTableViewCell.identifier, for: indexPath) as! IngredientsTableViewCell
        cell.configure(view_model: view_model.ingredients[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
