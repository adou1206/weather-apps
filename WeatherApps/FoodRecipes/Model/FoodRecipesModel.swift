//
//  FoodRecipesModel.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 26/03/2024.
//

import Foundation

struct FoodRecipesModel: Decodable, Equatable {
    let from: Int
    let to: Int
    let count: Int
    let links: NextLinks
    var hits: [Hit] = []
    
    enum CodingKeys: String, CodingKey {
        case from
        case to
        case count
        case links = "_links"
        case hits
    }
}

struct Hit: Decodable, Equatable {
    let recipe: Recipe
    let links: HitLinks
    
    enum CodingKeys: String, CodingKey {
        case recipe
        case links = "_links"
    }
}

struct HitLinks: Decodable, Equatable {
    var links_self: Next? = nil
    
    enum CodingKeys: String, CodingKey {
        case links_self = "self"
    }
}

struct NextLinks: Decodable, Equatable {
    var next: Next?
}

struct Next: Decodable, Equatable {
    let href: String
    let title: Title
}

struct Recipe: Decodable, Equatable {
    let uri: String
    let label: String
    let image: String
    let source: String
    let url: String
    let dietLabels: [String]
    let healthLabels: [String]
    let ingredientLines: [String]
    let ingredients: [Ingredient]
    let totalTime: Double
}

struct Ingredient: Decodable, Equatable {
    let text: String?
    let food: String?
    let image: String?
}

enum Title: String, Decodable, Equatable {
    case next_page = "Next page"
    case title_self = "Self"
}
