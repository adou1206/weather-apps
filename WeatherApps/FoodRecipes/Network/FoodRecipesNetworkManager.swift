//
//  FoodRecipesNetworkManager.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 26/03/2024.
//

import Foundation

protocol FoodRecipesNetworkManagerImpl {
    func request<U: Decodable>(
        session: URLSession,
        endpoint: FoodRecipesEndpoint,
        type: U.Type
    ) async throws -> U
    
    func request<U: Decodable>(
        session: URLSession,
        url_string: String,
        methodType: FoodRecipesEndpoint.MethodType,
        type: U.Type
    ) async throws -> U
}

final class FoodRecipesNetworkManager: FoodRecipesNetworkManagerImpl {
    static let shared = FoodRecipesNetworkManager()
    
    private init() {}
    
    func request<U>(session: URLSession, endpoint: FoodRecipesEndpoint, type: U.Type) async throws -> U where U : Decodable {
        guard let url = endpoint.url else {
            throw NetworkError.invalidURL
        }
//        print(url)
        let request = buildRequest(from: url, methodType: endpoint.methodType)
        
        let (data, response) = try await session.data(for: request)
//        print("decode response json: \(String(data: data, encoding: .utf8)!)")
        guard let response = response as? HTTPURLResponse,
              (200...300) ~= response.statusCode else {
            let statusCode = (response as! HTTPURLResponse).statusCode
            throw NetworkError.invalidStatusCode(statusCode: statusCode)
        }
        
        return try JSONDecoder().decode(U.self, from: data)
    }
    
    // For direct use url to access the data (Edamam Food Recipe API next page are return a new URL link)
    func request<U>(session: URLSession, url_string: String, methodType: FoodRecipesEndpoint.MethodType, type: U.Type) async throws -> U where U : Decodable {
        guard let url = URL(string: url_string) else {
            throw NetworkError.invalidURL
        }
//        print(url)
        let request = buildRequest(from: url, methodType: methodType)
        
        let (data, response) = try await session.data(for: request)
//        print("decode response json: \(String(data: data, encoding: .utf8)!)")
        guard let response = response as? HTTPURLResponse,
              (200...300) ~= response.statusCode else {
            let statusCode = (response as! HTTPURLResponse).statusCode
            throw NetworkError.invalidStatusCode(statusCode: statusCode)
        }
        
        return try JSONDecoder().decode(U.self, from: data)
    }
}

private extension FoodRecipesNetworkManager {
    func buildRequest(
        from url: URL,
        methodType: FoodRecipesEndpoint.MethodType
    ) -> URLRequest {
        var request = URLRequest(url: url)
        
        switch methodType {
        case .GET:
            request.httpMethod = "GET"
        }
        
        return request
    }
}
