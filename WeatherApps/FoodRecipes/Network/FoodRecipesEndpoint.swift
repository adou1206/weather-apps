//
//  Endpoint.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 26/03/2024.
//

import Foundation

enum FoodRecipesEndpoint {
    case detail(q: String?, cuisineType: [FoodRecipeCuisineTypes]?)
}

extension FoodRecipesEndpoint {
    enum MethodType: Equatable {
        case GET
    }
}

extension FoodRecipesEndpoint {
    var food_recipe_app_id: String {
        return "e56840f7"
    }
    
    var food_recipe_api_key: String {
        return "5ce50d45f0d9addc13e157cbf91a9509"
    }
    
    var host: String {
        return "api.edamam.com"
    }
    
    var methodType: MethodType {
        switch self {
        case .detail(_, _):
            return .GET
        }
    }
    
    var path: String {
        switch self {
        case .detail(_, _):
            return "/api/recipes/v2"
        }
    }
    
    var queryItems: [String: [String]] {
        switch self {
        case .detail(q: let q, cuisineType: let cuisineType):
            var parameter: [String : [String]] = [
                "type" : ["any"],
                "app_key": [food_recipe_api_key],
                "app_id": [food_recipe_app_id],
                "q": [q ?? ""],
                "field": [
                    "uri",
                    "label",
                    "image",
                    "source",
                    "url",
                    "dietLabels",
                    "healthLabels",
                    "ingredientLines",
                    "ingredients",
                    "totalTime"
                ],
            ]
            
            if let cuisineType = cuisineType {
                parameter["cuisineType"] = cuisineType.map { $0.string() }
            }
            
            return parameter
        }
    }
}

extension FoodRecipesEndpoint {
    var url: URL? {
        var urlComponent = URLComponents()
        urlComponent.scheme = "https"
        urlComponent.host = host
        urlComponent.path = path
        urlComponent.setQueryItems(with: queryItems)

        return urlComponent.url
    }
}
