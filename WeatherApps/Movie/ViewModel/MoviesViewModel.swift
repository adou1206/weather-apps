//
//  MoviesViewModel.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 22/08/2024.
//

import Foundation

final class MovieViewModel {
    private (set) var movieModel: MovieModel?
    private (set) var error: NetworkError?
    @Published private (set) var isLoading: Bool = false
    private (set) var hasError: Bool = false
    
    private let networkManager: MovieNetworkManagerImpl!
    
    init(
        networkManagerImpl: MovieNetworkManagerImpl = MovieNetworkManager.shared
    ) {
        self.networkManager = networkManagerImpl
    }
    
    @MainActor
    func fetchMovie(
        movie_title: String? = nil
    ) async {
        isLoading = true
        
        defer {
            isLoading = false
        }
        
        do {
            var endpoint: MovieEndpoint = .movie_list
            
            if let movie_title = movie_title {
                endpoint = .movie_search(movie_title: movie_title)
            }
            
            movieModel = try await networkManager.request(
                session: .shared,
                endpoint: endpoint,
                type: MovieModel.self
            )
        }catch {
            hasError = true
            
            if let networkError = error as? NetworkError {
                self.error = networkError
            }else {
                self.error = .custom(error: error)
            }
        }
    }
}
