//
//  MoviesNetworkManager.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 22/08/2024.
//

import Foundation

protocol MovieNetworkManagerImpl {
    func request<U: Decodable>(
        session: URLSession,
        endpoint: MovieEndpoint,
        type: U.Type
    ) async throws -> U
}

final class MovieNetworkManager: MovieNetworkManagerImpl {
    static let shared = MovieNetworkManager()
    
    private init() {}
    
    func request<U>(session: URLSession, endpoint: MovieEndpoint, type: U.Type) async throws -> U where U : Decodable {
        guard let request = buildRequest(endpoint: endpoint) else {
            throw NetworkError.invalidURL
        }
        
        let (data, response) = try await session.data(for: request)
        
        guard let response = response as? HTTPURLResponse,
              (200...300) ~= response.statusCode else {
            let statusCode = (response as! HTTPURLResponse).statusCode
            throw NetworkError.invalidStatusCode(statusCode: statusCode)
        }
        
        return try JSONDecoder().decode(U.self, from: data)
    }
}

private extension MovieNetworkManager {
    func buildRequest(
        endpoint: MovieEndpoint
    ) -> URLRequest? {
        guard let url = endpoint.url else {
            return nil
        }
        
        var request = URLRequest(url: url)
        
        switch endpoint.methodType {
        case .GET:
            request.httpMethod = "GET"
        }
        
        request.allHTTPHeaderFields = endpoint.headerQueryItems
        
        return request
    }
}
