//
//  NetworkingManagerUserDetailsResponseFailureMock.swift
//  WeatherAppsTests
//
//  Created by Tow Ching Shen on 26/02/2024.
//

#if DEBUG
import Foundation

class NetworkManagerWeatherResponseFailureMock: WeatherNetworkManagerImpl {
    func request<U>(session: URLSession, endpoint: WeatherEndpoint, type: U.Type) async throws -> U where U : Decodable {
        throw NetworkError.invalidURL
    }
}
#endif
