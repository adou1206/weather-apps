//
//  NetworkingManagerWeatherResponseSuccessMock.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 26/02/2024.
//

#if DEBUG
import Foundation

class NetworkManagerWeatherResponseSuccessMock: WeatherNetworkManagerImpl {
    func request<U>(session: URLSession, endpoint: WeatherEndpoint, type: U.Type) async throws -> U where U : Decodable {
        return try StaticJSONMapper.decode(file: "WeatherDetail", type: WeatherModel.self) as! U
    }
}
#endif
