//
//  MockURLSessionProtocol.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 22/02/2024.
//

import Foundation

class MockURLSessionProtocol: URLProtocol {
    static var loadingHandler: (() -> (HTTPURLResponse, Data?, Error?))?
    
    // allow us to actually control whether it can handle a given request
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    // return a fake version of the request
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    // control the mock response that we get back from this fake URLSession
    override func startLoading() {
        
        // ensure handler data are set
        guard let handler = MockURLSessionProtocol.loadingHandler else {
            fatalError("Loading handler is not set.")
        }
        
        let (response, data, error) = handler()
        
        // cacheStoragePolicy: .notAllowed make sure not cache any APIs data on device during test
        client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
        
        if let data {
            // send data back to URLSession
            client?.urlProtocol(self, didLoad: data)
        }
        
        if let error {
            client?.urlProtocol(self, didFailWithError: error)
        }
        
        // tell client finish loading
        client?.urlProtocolDidFinishLoading(self)
    }
    
    override func stopLoading() { }
}
