//
//  NetworkManagerFoodRecipeResponseFailureMock.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 24/04/2024.
//

#if DEBUG
import Foundation

class NetworkManagerFoodRecipeResponseFailureMock: FoodRecipesNetworkManagerImpl {
    func request<U>(session: URLSession, url_string: String, methodType: FoodRecipesEndpoint.MethodType, type: U.Type) async throws -> U where U : Decodable {
        throw NetworkError.invalidURL
    }
    
    func request<U>(session: URLSession, endpoint: FoodRecipesEndpoint, type: U.Type) async throws -> U where U : Decodable {
        throw NetworkError.invalidURL
    }
}
#endif

