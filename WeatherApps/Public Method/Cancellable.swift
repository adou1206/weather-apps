//
//  Cancellable.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 28/08/2024.
//

import Foundation

// refer: https://cocoacasts.com/image-caching-in-swift-cancelling-image-requests

protocol Cancellable {
    func cancel()
}

extension URLSessionTask: Cancellable { }
