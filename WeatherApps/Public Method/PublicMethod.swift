//
//  PublicMethod.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 18/06/2022.
//

import Foundation
import CoreLocation

class PublicMethod {
    static let shared = PublicMethod()
    
    func convertUnitTimestampToDate(timestamp: TimeInterval, format: String? = nil, isShowNow: Bool = true) -> String {
        let weather_date = Date(timeIntervalSince1970: timestamp)
        let date_formatter = DateFormatter()
        let calender = Calendar.current
        
        date_formatter.timeZone = .current
        date_formatter.locale = Locale.current
        
        if let format = format {
            let hour_result = calender.compare(Date(), to: weather_date, toGranularity: .hour)
            let day_result = calender.compare(Date(), to: weather_date, toGranularity: .day)
            
            if isShowNow, hour_result == .orderedSame, day_result == .orderedSame {
                return "Now"
            }
            
            date_formatter.dateFormat = format
        
            return date_formatter.string(from: weather_date)
        }
        
        date_formatter.dateStyle = .medium
        
        if calender.isDateInToday(weather_date) {
            return "Today"
        }
    
        return date_formatter.string(from: weather_date)
    }

    func makeTempatureDisplay(temp: Double) -> String {
        let measureFormatter = MeasurementFormatter()
        measureFormatter.unitStyle = .short
        
        let temp = Measurement(value: temp, unit: UnitTemperature.celsius)
        
        return measureFormatter.string(from: temp)
    }
    
    func showAddressWithCoordinate(location: CLLocation) async throws -> String {
        let geoCoder = CLGeocoder()
        
        var full_address: String = ""
        
        let placemarks = try await geoCoder.reverseGeocodeLocation(location)
        
        for placemark in placemarks {
//            if let name = placemark.name {
//                full_address += name
//            }
//            
//            if let postal_code = placemark.postalCode {
//                full_address += ", \(postal_code)"
//            }
//            
//            if let sub_administrative_area = placemark.subAdministrativeArea {
//                full_address += ", \(sub_administrative_area)"
//            }
//            
//            if let administrative_area = placemark.administrativeArea {
//                full_address += ", \(administrative_area)"
//            }
            
            if let administrative_area = placemark.administrativeArea {
                full_address += administrative_area
            }
            
            if let country = placemark.country {
                full_address += ", \(country)"
            }
        }
        
        return full_address
    }
    
    // Food Recipe
    
    func generateTimeDisplay(minutes: Double) -> String {
        guard minutes > 0 else { return "This Recipe didn't have actual time" }
        
        let new_minutes = (minutes - 10 < 1) ? 0 : minutes - 10
        
        return "\(new_minutes.convertMinutesToHrMinuteString()) - \(minutes.convertMinutesToHrMinuteString())"
    }
    
    // end Food Recipe
}
