//
//  StaticData.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 04/03/2024.
//

import Foundation

struct PublicStaticData {
    static var window_scene_screen_scale: CGFloat = 0.0
}
