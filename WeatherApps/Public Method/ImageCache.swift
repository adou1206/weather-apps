//
//  ImageCache.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 26/08/2024.
//

// refer: https://medium.com/@itsachin523/building-your-own-image-caching-in-ios-with-swift-d6a52912dd8f

import UIKit

class ImageCache {
    static let shared = ImageCache()
    private let cache = NSCache<NSString, UIImage>()
    
    private init() {}
    
    func getImage(forKey key: String) -> UIImage? {
        return cache.object(forKey: key as NSString)
    }
    
    func setImage(_ image: UIImage, forKey key: String) {
        cache.setObject(image, forKey: key as NSString)
    }
}
