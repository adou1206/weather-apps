//
//  ColorSets.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 29/02/2024.
//

import UIKit

enum ColorSets: String, CaseIterable {
    // Waether use
    case cloudy
    case mostly_clear
    case mostly_sunny
    case rainy
    case sunny
    case thunder
    case black_text
    case white_text
    case gradient_begin
    case gradient_end
    // Waether use
    
    // Food recipe use
    case food_recipe_button
    case food_recipe_text
    case food_recipe_selected
    case food_recipe_unselected
    case food_recipe_detail_background
    case food_recipe_detail_background2
    case food_recipe_gradient_begin
    case food_recipe_gradient_end
    // end Food recipe use
    
    // Movie use
    case movie_background
    // end Movie use
    
    func color() -> UIColor? {
        return UIColor.appColor(name: self)
    }
}
