//
//  FoodRecipeCuisineTypes.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 09/04/2024.
//

import UIKit

enum FoodRecipeCuisineTypes: String, CaseIterable {
    case american = "american"
    case asian = "asian"
    case british = "british"
    case caribbean = "caribbean"
    case central_europe = "central europe"
    case chinese = "chinese"
    case eastern_europe = "eastern europe"
    case french = "french"
    case greek = "greek"
    case indian = "indian"
    case italian = "italian"
    case japanese = "japanese"
    case korean = "korean"
    case kosher = "kosher"
    case mediterranean = "mediterranean"
    case mexican = "mexican"
    case middle_eastern = "middle eastern"
    case nordic = "nordic"
    case south_american = "south american"
    case south_east_asian = "south east asian"
    case world = "world"
    
    func string() -> String {
        return rawValue
    }
    
    func text() -> String {
        return string().capitalized
    }
    
    // all image icon refer on https://www.flaticon.com/
    func image() -> UIImage? {
        return UIImage(named: self.string())
    }
}
