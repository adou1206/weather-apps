//
//  NetworkError.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 22/08/2024.
//

import Foundation

enum NetworkError: LocalizedError, Equatable {
    case invalidURL
    case custom(error: Error)
    case invalidStatusCode(statusCode: Int)
    case invalidData
    case failedToDecode(error: Error)
    
    static func == (lhs: NetworkError, rhs: NetworkError) -> Bool {
        switch (lhs, rhs) {
        case (.invalidURL, .invalidURL):
            return true
            
        case (.custom(let lhsType), .custom(let rhsType)):
            return lhsType.localizedDescription == rhsType.localizedDescription
            
        case (.invalidStatusCode(let lhsType), .invalidStatusCode(let rhsType)):
            return lhsType == rhsType
            
        case (.invalidData, .invalidData):
            return true
            
        case (.failedToDecode(let lhsType), .failedToDecode(let rhsType)):
            return lhsType.localizedDescription == rhsType.localizedDescription
            
        default:
            return false
        }
    }
    
    var errorDescription: String? {
        switch self {
        case .invalidURL:
            return "URL isn't valid"
            
        case .custom(error: let error):
            return "Something went wrong.\n \(error.localizedDescription)"
            
        case .invalidStatusCode(_):
            return "Status code falls into the wrong range"
            
        case .invalidData:
            return "Response data is invalid"
            
        case .failedToDecode(_):
            return "Failed to decode"
        }
    }
}
