//
//  JobType.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 22/08/2024.
//

import Foundation

enum jobType: String, Decodable {
    case director = "Director"
}
