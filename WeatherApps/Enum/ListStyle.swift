//
//  ListStyle.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 05/04/2024.
//

import UIKit

enum ListStyle: CaseIterable {
    case weather_style
    case food_recipe_style
    case movie_style
    
    var vc: UIViewController {
        switch self {
        case .food_recipe_style:
            return FoodRecipeViewController()
            
        case .weather_style:
            return WeatherViewController()
            
        case .movie_style:
            return MovieViewController()
        }
    }
    
    func gradientLayer(in frame: CGRect) -> CAGradientLayer {
        return CAGradientLayer.gradientLayer(for: self, in: frame)
    }
    
    func navigationTitleFont() -> UIFont {
        switch self {
        case .weather_style:
            return UIFont.montserratBold(size: 20)
            
        case .food_recipe_style:
            return UIFont.latoSemiBold(size: 20)
            
        case .movie_style:
            return .systemFont(ofSize: 20)
        }
    }
    
    func navigationBarButtonItemFont() -> UIFont {
        switch self {
        case .weather_style:
            return UIFont.montserratRegular(size: 15)
            
        case .food_recipe_style:
            return UIFont.latoRegular(size: 15)
            
        case .movie_style:
            return .systemFont(ofSize: 15)
        }
    }
    
    func titleText() -> String {
        switch self {
        case .weather_style:
            return "Weather"
            
        case .food_recipe_style:
            return "Food Recipe"
            
        case .movie_style:
            return "Movie"
        }
    }
}
