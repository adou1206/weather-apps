//
//  FoodRecipeListSection.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 02/04/2024.
//

import Foundation

enum FoodRecipeListSection {
    case recipe_category([String])
    case recipe_detail([Recipe])
    
    var category_item: [String] {
        switch self {
        case .recipe_category(let category_list):
            return category_list
            
        default:
            return []
        }
    }
    
    var detail_items: [Recipe] {
        switch self {
        case .recipe_detail(let recipe_list):
            return recipe_list
            
        default:
            return []
        }
    }
    
    var count: Int {
        switch self {
        case .recipe_category(_):
            return category_item.count
            
        case .recipe_detail(_):
            return detail_items.count
        }
    }
    
    var title: String {
        switch self {
        case .recipe_category:
            return "Choose Category"
            
        case .recipe_detail:
            return "Recipe"
        }
    }
}
