//
//  WeatherType.swift
//  WeatherApps
//
//  Created by Tow Ching Shen on 01/03/2024.
//

import Foundation
import UIKit

enum WeatherType: String, CaseIterable {
    case cloudy
    case mostly_clear
    case mostly_sunny
    case rainy
    case sunny
    case thunder
    
    static func weatherTypeFromString(main: String, clouds: Int) -> Self {
        switch main.lowercased() {
        case "rain":
            return .rainy
            
        case "thunderstorm":
            return .thunder
            
        case "clear":
            return .sunny
            
        case "clouds":
            return clouds < 50 ? .mostly_clear : .cloudy
            
        default:
            return .mostly_clear
        }
    }
    
    func displayColor() -> UIColor? {
        switch self {
        case .cloudy:
            return ColorSets.cloudy.color()
            
        case .mostly_clear:
            return ColorSets.mostly_clear.color()
            
        case .mostly_sunny:
            return ColorSets.mostly_sunny.color()
            
        case .rainy:
            return ColorSets.rainy.color()
            
        case .sunny:
            return ColorSets.sunny.color()
            
        case .thunder:
            return ColorSets.thunder.color()
        }
    }
    
    func displayText() -> String {
        return self.rawValue.replacingOccurrences(of: "_", with: " ")
    }
    
    func displayTextColor() -> UIColor? {
        switch self {
        case .sunny, .mostly_sunny, .rainy:
            return ColorSets.black_text.color()
            
        default:
            return ColorSets.white_text.color()
        }
    }
}
