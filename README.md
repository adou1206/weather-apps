# This application inside have 3 kind of display (Weather, Food Recipe, and Movie).

## Weather

[Sample video](https://gitlab.com/adou1206/weather-apps/-/blob/main/WeatherDisplay.mp4)

## Food Recipe

[Sample video 1](https://gitlab.com/adou1206/weather-apps/-/blob/main/FoodRecipeDisplay1.mp4)

[Sample video 2](https://gitlab.com/adou1206/weather-apps/-/blob/main/FoodRecipeDisplay2.mp4)

[Sample video 3](https://gitlab.com/adou1206/weather-apps/-/blob/main/FoodRecipeDisplay3.mp4)

## Movie

[Sample video](https://gitlab.com/adou1206/weather-apps/-/blob/main/MovieDisplay.mp4)
