//
//  FoodRecipeViewModelSuccessTests.swift
//  WeatherAppsTests
//
//  Created by Tow Ching Shen on 23/04/2024.
//

import XCTest
@testable import WeatherApps

final class FoodRecipeViewModelSuccessTests: XCTestCase {
    private var networkMock: FoodRecipesNetworkManagerImpl!
    private var vm: FoodRecipesViewModel!
    
    override func setUp() {
        networkMock = NetworkManagerFoodRecipeResponseSuccessMock()
        vm = FoodRecipesViewModel(networkManagerImpl: networkMock)
    }
    
    override func tearDown() {
        networkMock = nil
        vm = nil
    }
    
    func test_fetchFoodRecipes_with_successful_response_food_recipe_details_is_set() async throws {
        XCTAssertFalse(vm.isLoading, "The Food Recipes view model shouldn't be loading any data")
        
        defer {
            XCTAssertFalse(vm.isLoading, "The Food Recipes view model shouldn't be loading any data")
        }
        
        await vm.fetchFoodRecipes(for: "Chicken", with: [.american])
        
        XCTAssertGreaterThan(vm.hits.count, 0, "The Food Recipes view model hits count should not be less than 0")
        XCTAssertNotNil(vm.next, "The Food Recipes view model next should not be nil")
        
        let foodRecipesDetailsData = try StaticJSONMapper.decode(file: "FoodRecipeDetail", type: FoodRecipesModel.self)
        
        XCTAssertEqual(
            vm.hits,
            foodRecipesDetailsData.hits,
            "The Food Recipes view model hits response from our netwrk mock should match"
        )
        XCTAssertEqual(
            vm.next,
            foodRecipesDetailsData.links.next,
            "The Food Recipes view model next response from our netwrk mock should match"
        )
    }
    
    func test_fetchFoodRecipesNext_with_successful_response_food_recipe_details_is_set() async throws {
        XCTAssertFalse(vm.isLoading, "The Food Recipes view model shouldn't be loading any data")
        
        defer {
            XCTAssertFalse(vm.isLoading, "The Food Recipes view model shouldn't be loading any data")
        }
        
        await vm.fetchFoodRecipesNext(
            herf: "https://api.edamam.com/api/recipes/v2?q=&app_key=5ce50d45f0d9addc13e157cbf91a9509&field=dietLabels&field=healthLabels&field=image&field=ingredientLines&field=ingredients&field=label&field=source&field=totalTime&field=uri&field=url&_cont=CHcVQBtNNQphDmgVQntAEX4BY0t3AwAVX3dBVjNFYgdwDQZTFWVEAjQXMlZ2AQYDETZABmRBNQchVRFqX3cWQT1OcV9xBE4%3D&cuisineType=american&type=any&app_id=e56840f7"
        )
        
        XCTAssertGreaterThan(vm.hits.count, 0, "The Food Recipes view model hits count should not be less than 0")
        XCTAssertNotNil(vm.next, "The Food Recipes view model next should not be nil")
        
        let foodRecipesDetailsData = try StaticJSONMapper.decode(file: "FoodRecipeDetail", type: FoodRecipesModel.self)
        
        XCTAssertEqual(
            vm.hits,
            foodRecipesDetailsData.hits,
            "The Food Recipes view model hits response from our netwrk mock should match"
        )
        XCTAssertEqual(
            vm.next,
            foodRecipesDetailsData.links.next,
            "The Food Recipes view model next response from our netwrk mock should match"
        )
    }
}
