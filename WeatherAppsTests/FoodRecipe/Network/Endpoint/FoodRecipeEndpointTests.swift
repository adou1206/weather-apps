//
//  FoodRecipeEndpointTests.swift
//  WeatherAppsTests
//
//  Created by Tow Ching Shen on 23/04/2024.
//

import XCTest
@testable import WeatherApps

final class FoodRecipeEndpointTests: XCTestCase {
    func test_with_detail_endpoint_request_is_valid() {
        let q: String = "Chicken"
        let cuisine_type: [FoodRecipeCuisineTypes] = [.american]
        let endpoint: FoodRecipesEndpoint = .detail(q: q, cuisineType: cuisine_type)
        
        XCTAssertEqual(endpoint.host, "api.edamam.com", "The host should be api.edamam.com")
        XCTAssertEqual(endpoint.path, "/api/recipes/v2", "The path should be /api/recipes/v2")
        XCTAssertEqual(endpoint.methodType, .GET, "The methodType should be .GET")
        XCTAssertEqual(
            endpoint.queryItems,
            [
                "type" : ["any"],
                "app_key": [endpoint.food_recipe_api_key],
                "app_id": [endpoint.food_recipe_app_id],
                "q": [q],
                "field": [
                    "uri",
                    "label",
                    "image",
                    "source",
                    "url",
                    "dietLabels",
                    "healthLabels",
                    "ingredientLines",
                    "ingredients",
                    "totalTime"
                ],
                "cuisineType": [
                    "american"
                ]
            ],
            "The queryItems should be \(["app_id": ["e56840f7"], "q": ["Chicken"], "field": ["uri", "label", "image", "source", "url", "dietLabels", "healthLabels", "ingredientLines", "ingredients", "totalTime"], "cuisineType": ["american"], "type": ["any"], "app_key": ["5ce50d45f0d9addc13e157cbf91a9509"]])"
        )
        
        XCTAssertEqual(
            endpoint.url?.absoluteString,
            "https://api.edamam.com/api/recipes/v2?app_id=e56840f7&app_key=5ce50d45f0d9addc13e157cbf91a9509&cuisineType=american&field=dietLabels&field=healthLabels&field=image&field=ingredientLines&field=ingredients&field=label&field=source&field=totalTime&field=uri&field=url&q=Chicken&type=any",
            "The generated doesn't match our endpoint (FoodRecipe)"
        )
    }
}
