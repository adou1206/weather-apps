//
//  FoodRecipeNetworkManagerTests.swift
//  WeatherAppsTests
//
//  Created by Tow Ching Shen on 23/04/2024.
//

import XCTest
@testable import WeatherApps

final class FoodRecipeNetworkManagerTests: XCTestCase {
    private var session: URLSession!
    private var url: URL!
    
    override func setUp() {
        url = URL(string: "https://api.edamam.com/api")
        
        // each and every single one of tests want to make sure that the state of them is clean, so don't want have any data that's been cached on the device between tests
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLSessionProtocol.self]
        session = URLSession(configuration: configuration)
    }
    
    override func tearDown() {
        url = nil
        session = nil
    }

    func test_requestEndpoint_with_successful_response_response_is_valid() async throws {
        guard let path = Bundle.main.path(forResource: "FoodRecipeDetail", ofType: "json"),
              let data = FileManager.default.contents(atPath: path) else {
            XCTFail("Failed to get the FoodRecipeDetail file")
            return
        }
        
        MockURLSessionProtocol.loadingHandler = {
            let response = HTTPURLResponse(
                url: self.url,
                statusCode: 200,
                httpVersion: nil,
                headerFields: nil
            )
            
            return (response!, data, nil)
        }
        
        let res = try await FoodRecipesNetworkManager.shared.request(
            session: session,
            endpoint: .detail(q: "", cuisineType: []),
            type: FoodRecipesModel.self
        )

        let staticJSON = try StaticJSONMapper.decode(file: "FoodRecipeDetail", type: FoodRecipesModel.self)

        XCTAssertEqual(res, staticJSON, "The returned Food Recipe response should be decoded properly")
    }
    
    func test_requestEndpoint_with_unsucessful_response_code_in_invalid_range_is_invalid() async {
        let invalidStatusCode: Int = 400
        
        MockURLSessionProtocol.loadingHandler = {
            let response = HTTPURLResponse(
                url: self.url,
                statusCode: invalidStatusCode,
                httpVersion: nil,
                headerFields: nil
            )
            
            return (response!, nil, nil)
        }
        
        do {
            _ = try await FoodRecipesNetworkManager.shared.request(
                session: session,
                endpoint: .detail(q: "", cuisineType: []),
                type: FoodRecipesModel.self
            )
            
        }catch {
            guard let networkError = error as? NetworkError else {
                XCTFail("Got the wrong type of error, expecting Food Recipes NetworkingManager NetworkingError")
                return
            }
            
            XCTAssertEqual(
                networkError,
                NetworkError.invalidStatusCode(statusCode: invalidStatusCode),
                "Error should be a Food Recipes networking error which throws an invalid status code"
            )
        }
    }
    
    func test_requestEndpoint_with_successful_response_with_invalid_json_is_invalid() async {
        guard let path = Bundle.main.path(forResource: "FoodRecipeDetail", ofType: "json"),
              let data = FileManager.default.contents(atPath: path) else {
            XCTFail("Failed to get the FoodRecipeDetail file")
            return
        }
        
        MockURLSessionProtocol.loadingHandler = {
            let response = HTTPURLResponse(
                url: self.url,
                statusCode: 200,
                httpVersion: nil,
                headerFields: nil
            )
            
            return (response!, data, nil)
        }
        
        do {
            _ = try await FoodRecipesNetworkManager.shared.request(
                session: session,
                endpoint: .detail(q: "", cuisineType: []),
                type: Hit.self
            )
            
        }catch {
            if error is NetworkError {
                XCTFail("The error should be a system decoding error (Food Recipes)")
            }
        }
    }
}
