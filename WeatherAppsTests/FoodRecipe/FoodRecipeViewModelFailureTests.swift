//
//  FoodRecipeViewModelFailureTests.swift
//  WeatherAppsTests
//
//  Created by Tow Ching Shen on 23/04/2024.
//

import XCTest
@testable import WeatherApps

final class FoodRecipeViewModelFailureTests: XCTestCase {
    private var networkMock: FoodRecipesNetworkManagerImpl!
    private var vm: FoodRecipesViewModel!

    override func setUp() {
        networkMock = NetworkManagerFoodRecipeResponseFailureMock()
        vm = FoodRecipesViewModel(networkManagerImpl: networkMock)
    }
    
    override func tearDown() {
        networkMock = nil
        vm = nil
    }
    
    func test_fetchFoodRecipes_with_unsuccessful_response_error_is_handled() async {
        XCTAssertFalse(vm.isLoading, "The Food Recipes view model shouldn't be loading any data")
        
        defer {
            XCTAssertFalse(vm.isLoading, "The Food Recipes view model shouldn't be loading any data")
        }
        
        await vm.fetchFoodRecipes(for: "Chicken", with: [.american])
        
        XCTAssertTrue(vm.hasError, "The Food Recipes view model error should be true")
        
        XCTAssertNotNil(vm.error, "The Food Recipes view model error should not be nil")
    }
    
    func test_fetchFoodRecipesNext_with_unsuccessful_response_error_is_handled() async {
        XCTAssertFalse(vm.isLoading, "The Food Recipes view model shouldn't be loading any data")
        
        defer {
            XCTAssertFalse(vm.isLoading, "The Food Recipes view model shouldn't be loading any data")
        }
        
        await vm.fetchFoodRecipesNext(
            herf: "https://api.edamam.com/api/recipes/v2?q=&app_key=5ce50d45f0d9addc13e157cbf91a9509&field=dietLabels&field=healthLabels&field=image&field=ingredientLines&field=ingredients&field=label&field=source&field=totalTime&field=uri&field=url&_cont=CHcVQBtNNQphDmgVQntAEX4BY0t3AwAVX3dBVjNFYgdwDQZTFWVEAjQXMlZ2AQYDETZABmRBNQchVRFqX3cWQT1OcV9xBE4%3D&cuisineType=american&type=any&app_id=e56840f7"
        )
        
        XCTAssertTrue(vm.hasError, "The Food Recipes view model error should be true")
        
        XCTAssertNotNil(vm.error, "The Food Recipes view model error should not be nil")
    }
}
