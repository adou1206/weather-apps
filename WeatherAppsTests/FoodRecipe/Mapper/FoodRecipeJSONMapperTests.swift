//
//  FoodRecipeJSONMapperTests.swift
//  WeatherAppsTests
//
//  Created by Tow Ching Shen on 23/04/2024.
//

import XCTest
@testable import WeatherApps

final class FoodRecipeJSONMapperTests: XCTestCase {
    func test_with_valid_json_successfully_decodes() {
        XCTAssertNoThrow(try StaticJSONMapper.decode(file: "FoodRecipeDetail", type: FoodRecipesModel.self), "Mapper shouldn't throw an error (FoodRecipesModel)")
        
        let food_recipe_detail = try? StaticJSONMapper.decode(file: "FoodRecipeDetail", type: FoodRecipesModel.self)
        
        XCTAssertNotNil(food_recipe_detail, "Food Recipe detail shouldn't be nil")
            
        XCTAssertEqual(food_recipe_detail?.count, 10000, "Count should be 10000")
        XCTAssertEqual(food_recipe_detail?.from, 1, "From should be 1")
        XCTAssertEqual(food_recipe_detail?.to, 20, "To should be 20")
        XCTAssertEqual(food_recipe_detail?.hits.count, 20, "Hits count should be 20")
        XCTAssertNotNil(food_recipe_detail?.links, "Links shouldn't be nil")
        
        XCTAssertNotNil(food_recipe_detail?.links.next, "Links next shouldn't be nil")
        XCTAssertEqual(food_recipe_detail?.links.next?.title, Title.next_page, "Links Next title should be Title.next_page")
        XCTAssertEqual(
            food_recipe_detail?.links.next?.href, "https://api.edamam.com/api/recipes/v2?q=&app_key=5ce50d45f0d9addc13e157cbf91a9509&field=dietLabels&field=healthLabels&field=image&field=ingredientLines&field=ingredients&field=label&field=source&field=totalTime&field=uri&field=url&_cont=CHcVQBtNNQphDmgVQntAEX4BY0t3AwAVX3dBVjNFYgdwDQZTFWVEAjQXMlZ2AQYDETZABmRBNQchVRFqX3cWQT1OcV9xBE4%3D&cuisineType=american&type=any&app_id=e56840f7",
            "Links Next Title should be https://api.edamam.com/api/recipes/v2?q=&app_key=5ce50d45f0d9addc13e157cbf91a9509&field=dietLabels&field=healthLabels&field=image&field=ingredientLines&field=ingredients&field=label&field=source&field=totalTime&field=uri&field=url&_cont=CHcVQBtNNQphDmgVQntAEX4BY0t3AwAVX3dBVjNFYgdwDQZTFWVEAjQXMlZ2AQYDETZABmRBNQchVRFqX3cWQT1OcV9xBE4%3D&cuisineType=american&type=any&app_id=e56840f7"
        )
        
        XCTAssertNotNil(food_recipe_detail?.hits[0].links, "Hits[0] links shouldn't be nil")
        XCTAssertNotNil(food_recipe_detail?.hits[0].links.links_self, "Hits[0] Links links_self shouldn't be nil")
        XCTAssertEqual(
            food_recipe_detail?.hits[0].links.links_self?.title,
            Title.title_self,
            "Hits[0] Links Links_self title should be Title.title_self"
        )
        XCTAssertEqual(
            food_recipe_detail?.hits[0].links.links_self?.href,
            "https://api.edamam.com/api/recipes/v2/4bb99424e1bbc40d3cd1d891883d6745?type=public&app_id=e56840f7&app_key=5ce50d45f0d9addc13e157cbf91a9509",
            "Hits[0] Links Links_self title should be https://api.edamam.com/api/recipes/v2/4bb99424e1bbc40d3cd1d891883d6745?type=public&app_id=e56840f7&app_key=5ce50d45f0d9addc13e157cbf91a9509"
        )
        
        XCTAssertNotNil(food_recipe_detail?.hits[0].recipe, "Hits[0] recipe shouldn't be nil")
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.uri,
            "http://www.edamam.com/ontologies/edamam.owl#recipe_4bb99424e1bbc40d3cd1d891883d6745",
            "Hits[0] Recipe uri should be http://www.edamam.com/ontologies/edamam.owl#recipe_4bb99424e1bbc40d3cd1d891883d6745"
        )
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.label,
            "Frothy Iced Matcha Green Tea Recipe",
            "Hits[0] Recipe label should be Frothy Iced Matcha Green Tea Recipe"
        )
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.image,
            "https://edamam-product-images.s3.amazonaws.com/web-img/643/643d4bad9cc21284f7f52b1b9b862848.jpg?X-Amz-Security-Token=IQoJb3JpZ2luX2VjECgaCXVzLWVhc3QtMSJGMEQCIGbhyRqMcSUP%2FmG0QMVoaediM%2FTNgif27zz8nWk3ly4MAiBGl7WbvkkotkbILZEMTgH0RvZaUQz%2FXwRePR4L7kGb5Cq5BQhwEAAaDDE4NzAxNzE1MDk4NiIM0lNNHvikGxebJ3%2BxKpYF4%2BLWLXApJ%2FxCkRKGBboYxB4eMp0kwrlax6KNEn%2BOsG9O%2BY4MPsl1AqU34mn1ZqT4PGlAowiWwpowVT%2FatKmBGRVTfJMWFX7H0xCEDz2kVpjTLRs%2FC0%2F4UdTYLpQxI6iHwOOxvZ1nROHlEIEHxYm7fl5F%2FClVhuXE3EbPoyoGvYZT271r4Hc%2BdO1VrNsHrTFiSfeuboJSvB8EwC%2BnXKNYyZlJDKFa38qQvgCcYONAIGE8N887MnvakUBKnHz8qWI7Tox5foOpGXKMYPLwLN%2Bhyejh35SLtXvXBBzo6bm0BfeeL9QFsSgaa%2FMWNZKIfDEzOogYNMw2fNvAZQk%2BMLEiv6xFU2hC5eIHl1PI8tUMU7JqgTiKTlbRAjvJf0TsE40N4vAtAwdFpJl1Mf1MrZ1x7EL7JtH3hUjhP2zfsbpmxHh85SeagZEmlhnt1qYlIEbvP7JpoJwmsyX5pSEYAUTZkF9TWTzz43Svim0FRVDOe3JESslvr2F%2BbuO%2BPRgc9aMa6r21sf4ju0a%2FLh3iWqlC%2BOpQEgkHnRbgmbIWxJjC%2BB5GlpTZbaP9gecWk8ewlypEl2HyTlDd5ksQhIOMOtiQXOz26ZKEcUsHMWX8pR6hitqGveCqb3CYGdsF2qp7ICTRTCtyxuU%2FB6HXIV9jt2Q1CG15cwQyy9IaNuxlTmImBSL%2BTgFMtDThnBODdq%2FaZKAdCiuQXNtz1lVRmuAC%2B%2FlKiKaokVTUZR3g%2FrFkqwrpef3k4GZsknp0gA1VDjKg3m8od67begdLwa1LcxouDG6bB4jvtg%2BkQB6i2L7jogFqfPgpBMGtt2NoSKZc4qxXGIBVprVr6MsfuK9u9bPsqQTwGLkUuqYWa5YvLqJtk3iFWBUVcmuD0D8wwcCdsQY6sgFCdY9Tt0XPIT5bapWWWHTifttotAG74jrLG%2BW3w5XXc73EwgUc7E1F6VqiZZY2QakdiPNj3zlYQLa2xuCf5T8tl%2FoFpRDhcEZ8qLcmPZJs1CmTyq2%2F%2BUr2SVx9%2BVjopneEH82JO4hpsjucowwH5ZReIoxstb3gizwad31pytFvRiKK9%2F%2B%2Flo1bJqTA6dSzzax5U%2BiYngJuxfYFJLHHFoRV4dnHvsbachj%2F87Xte0C3YOQf&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20240423T085742Z&X-Amz-SignedHeaders=host&X-Amz-Expires=3600&X-Amz-Credential=ASIASXCYXIIFNPDYW3ML%2F20240423%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=5c9fddef2f2a198c0368af858322a816b729edc17ef871fb106bc70ca208f4f3",
            "Hits Recipe image should be https://edamam-product-images.s3.amazonaws.com/web-img/643/643d4bad9cc21284f7f52b1b9b862848.jpg?X-Amz-Security-Token=IQoJb3JpZ2luX2VjECgaCXVzLWVhc3QtMSJGMEQCIGbhyRqMcSUP%2FmG0QMVoaediM%2FTNgif27zz8nWk3ly4MAiBGl7WbvkkotkbILZEMTgH0RvZaUQz%2FXwRePR4L7kGb5Cq5BQhwEAAaDDE4NzAxNzE1MDk4NiIM0lNNHvikGxebJ3%2BxKpYF4%2BLWLXApJ%2FxCkRKGBboYxB4eMp0kwrlax6KNEn%2BOsG9O%2BY4MPsl1AqU34mn1ZqT4PGlAowiWwpowVT%2FatKmBGRVTfJMWFX7H0xCEDz2kVpjTLRs%2FC0%2F4UdTYLpQxI6iHwOOxvZ1nROHlEIEHxYm7fl5F%2FClVhuXE3EbPoyoGvYZT271r4Hc%2BdO1VrNsHrTFiSfeuboJSvB8EwC%2BnXKNYyZlJDKFa38qQvgCcYONAIGE8N887MnvakUBKnHz8qWI7Tox5foOpGXKMYPLwLN%2Bhyejh35SLtXvXBBzo6bm0BfeeL9QFsSgaa%2FMWNZKIfDEzOogYNMw2fNvAZQk%2BMLEiv6xFU2hC5eIHl1PI8tUMU7JqgTiKTlbRAjvJf0TsE40N4vAtAwdFpJl1Mf1MrZ1x7EL7JtH3hUjhP2zfsbpmxHh85SeagZEmlhnt1qYlIEbvP7JpoJwmsyX5pSEYAUTZkF9TWTzz43Svim0FRVDOe3JESslvr2F%2BbuO%2BPRgc9aMa6r21sf4ju0a%2FLh3iWqlC%2BOpQEgkHnRbgmbIWxJjC%2BB5GlpTZbaP9gecWk8ewlypEl2HyTlDd5ksQhIOMOtiQXOz26ZKEcUsHMWX8pR6hitqGveCqb3CYGdsF2qp7ICTRTCtyxuU%2FB6HXIV9jt2Q1CG15cwQyy9IaNuxlTmImBSL%2BTgFMtDThnBODdq%2FaZKAdCiuQXNtz1lVRmuAC%2B%2FlKiKaokVTUZR3g%2FrFkqwrpef3k4GZsknp0gA1VDjKg3m8od67begdLwa1LcxouDG6bB4jvtg%2BkQB6i2L7jogFqfPgpBMGtt2NoSKZc4qxXGIBVprVr6MsfuK9u9bPsqQTwGLkUuqYWa5YvLqJtk3iFWBUVcmuD0D8wwcCdsQY6sgFCdY9Tt0XPIT5bapWWWHTifttotAG74jrLG%2BW3w5XXc73EwgUc7E1F6VqiZZY2QakdiPNj3zlYQLa2xuCf5T8tl%2FoFpRDhcEZ8qLcmPZJs1CmTyq2%2F%2BUr2SVx9%2BVjopneEH82JO4hpsjucowwH5ZReIoxstb3gizwad31pytFvRiKK9%2F%2B%2Flo1bJqTA6dSzzax5U%2BiYngJuxfYFJLHHFoRV4dnHvsbachj%2F87Xte0C3YOQf&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20240423T085742Z&X-Amz-SignedHeaders=host&X-Amz-Expires=3600&X-Amz-Credential=ASIASXCYXIIFNPDYW3ML%2F20240423%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=5c9fddef2f2a198c0368af858322a816b729edc17ef871fb106bc70ca208f4f3"
        )
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.source,
            "Serious Eats",
            "Hits[0] Recipe source should be Serious Eats"
        )
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.url,
            "http://www.seriouseats.com/recipes/2016/08/iced-matcha-green-tea-recipe.html",
            "Hits[0] Recipe url should be http://www.seriouseats.com/recipes/2016/08/iced-matcha-green-tea-recipe.html"
        )
        XCTAssertEqual(food_recipe_detail?.hits[0].recipe.dietLabels.count, 3, "Hits[0] Recipe DietLabels count should be 3")
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.dietLabels,
            [
                "High-Protein",
                "Low-Fat",
                "Low-Sodium"
            ],
            "Hits[0] Recipe dietLabels should be \(["High-Protein", "Low-Fat", "Low-Sodium"])"
        )
        XCTAssertEqual(food_recipe_detail?.hits[0].recipe.healthLabels.count, 28, "Hits[0] Recipe HealthLabels count should be 28")
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.healthLabels,
            [
                "Sugar-Conscious",
                "Low Sugar",
                "Low Potassium",
                "Kidney-Friendly",
                "Keto-Friendly",
                "Vegan",
                "Vegetarian",
                "Pescatarian",
                "Dairy-Free",
                "Gluten-Free",
                "Wheat-Free",
                "Egg-Free",
                "Peanut-Free",
                "Tree-Nut-Free",
                "Soy-Free",
                "Fish-Free",
                "Shellfish-Free",
                "Pork-Free",
                "Red-Meat-Free",
                "Crustacean-Free",
                "Celery-Free",
                "Mustard-Free",
                "Sesame-Free",
                "Lupine-Free",
                "Mollusk-Free",
                "Alcohol-Free",
                "No oil added",
                "Kosher"
            ],
            "Hits[0] Recipe healthLabels should be \(["Sugar-Conscious", "Low Sugar", "Low Potassium", "Kidney-Friendly", "Keto-Friendly", "Vegan", "Vegetarian", "Pescatarian", "Dairy-Free", "Gluten-Free", "Wheat-Free", "Egg-Free", "Peanut-Free", "Tree-Nut-Free", "Soy-Free", "Fish-Free", "Shellfish-Free", "Pork-Free", "Red-Meat-Free", "Crustacean-Free", "Celery-Free", "Mustard-Free", "Sesame-Free", "Lupine-Free", "Mollusk-Free", "Alcohol-Free", "No oil added", "Kosher"])"
        )
        XCTAssertEqual(food_recipe_detail?.hits[0].recipe.ingredientLines.count, 2, "Hits[0] Recipe IngredientLines count should be 2")
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.ingredientLines,
            [
                "2 teaspoons (6g) Japanese matcha green tea (see note above)",
                "8 ounces (235ml) cold water"
            ],
            "Hits[0] Recipe ingredientLines should be \(["2 teaspoons (6g) Japanese matcha green tea (see note above)", "8 ounces (235ml) cold water"])"
        )
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.ingredients.count,
            2,
            "Hits[0] Recipe IngredientLines count should be 2"
        )
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.ingredients[0].food,
            "green tea",
            "Hits[0] Recipe IngredientLines[0] food should be green tea"
        )
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.ingredients[0].image,
            "https://www.edamam.com/food-img/db9/db99a766fad87e02b4eac4840daaeaad.jpg",
            "Hits[0] Recipe IngredientLines[0] image should be https://www.edamam.com/food-img/db9/db99a766fad87e02b4eac4840daaeaad.jpg"
        )
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.ingredients[0].text,
            "2 teaspoons (6g) Japanese matcha green tea (see note above)",
            "Hits[0] Recipe IngredientLines[0] text should be 2 teaspoons (6g) Japanese matcha green tea (see note above)"
        )
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.ingredients[1].food,
            "water",
            "Hits[0] Recipe IngredientLines[1] food should be water"
        )
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.ingredients[1].image,
            "https://www.edamam.com/food-img/5dd/5dd9d1361847b2ca53c4b19a8f92627e.jpg",
            "Hits[0] Recipe IngredientLines[1] image should be https://www.edamam.com/food-img/5dd/5dd9d1361847b2ca53c4b19a8f92627e.jpg"
        )
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.ingredients[1].text,
            "8 ounces (235ml) cold water",
            "Hits[0] Recipe IngredientLines[1] text should be 8 ounces (235ml) cold water"
        )
        XCTAssertEqual(
            food_recipe_detail?.hits[0].recipe.totalTime,
            2,
            "Hits[0] Recipe totalTime should be 2"
        )
    }
    
    func test_with_missing_file_error_thrown() {
        XCTAssertThrowsError(try StaticJSONMapper.decode(file: "", type: FoodRecipesModel.self), "An error should be thrown (FoodRecipesModel)")
        
        do {
            _ = try StaticJSONMapper.decode(file: "", type: FoodRecipesModel.self)
            
        }catch {
            guard let mappingError = error as? StaticJSONMapper.MappingError else {
                XCTFail("This is the wrong type of error for missing files (FoodRecipesModel)")
                return
            }
            
            XCTAssertEqual(
                mappingError,
                StaticJSONMapper.MappingError.failedToGetContents,
                "This should be a failed to get contents error (FoodRecipesModel)"
            )
        }
    }
    
    func test_with_invalid_file_error_thrown() {
        XCTAssertThrowsError(try StaticJSONMapper.decode(file: "dfddf", type: FoodRecipesModel.self), "An error should be thrown (FoodRecipesModel)")
        
        do {
            _ = try StaticJSONMapper.decode(file: "dfddf", type: FoodRecipesModel.self)
            
        }catch {
            guard let mappingError = error as? StaticJSONMapper.MappingError else {
                XCTFail("This is the wrong type of error for missing files (FoodRecipesModel)")
                return
            }
            
            XCTAssertEqual(mappingError,
                           StaticJSONMapper.MappingError.failedToGetContents,
                           "This should be a failed to get contents error (FoodRecipesModel)"
            )
        }
    }
}
