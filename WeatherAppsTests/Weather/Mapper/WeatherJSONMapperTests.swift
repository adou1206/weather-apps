//
//  JSONMapperTests.swift
//  WeatherAppsTests
//
//  Created by Tow Ching Shen on 26/02/2024.
//

import XCTest
@testable import WeatherApps

final class JSONMapperTests: XCTestCase {
    func test_with_valid_json_successfully_decodes() {
        XCTAssertNoThrow(try StaticJSONMapper.decode(file: "WeatherDetail", type: WeatherModel.self), "Mapper shouldn't throw an error (WeatherModel)")
        
        let weather_detail = try? StaticJSONMapper.decode(file: "WeatherDetail", type: WeatherModel.self)
        
        XCTAssertNotNil(weather_detail, "Weather detail shouldn't be nil")
            
        XCTAssertEqual(weather_detail?.timezone, "Asia/Kuala_Lumpur", "Timezone should be Asia/Kuala_Lumpur")
        
        XCTAssertEqual(weather_detail?.current?.temp, 32.68, "Current temp should be 32.68")
        XCTAssertEqual(weather_detail?.current?.feels_like, 39.68, "Current feels_like should be 39.68")
        XCTAssertEqual(weather_detail?.current?.weather?[0].icon, "03d", "Current weather icon should be 03d")
        XCTAssertEqual(weather_detail?.current?.weather?[0].main, "Clouds", "Current weather main should be Clouds")
        XCTAssertEqual(weather_detail?.current?.clouds, 40, "Current clouds should be 40")
        
        XCTAssertEqual(weather_detail?.daily?.count, 8, "Daily count should be 8")
        XCTAssertEqual(weather_detail?.hourly?.count, 48, "Hourly count should be 48")
        
        XCTAssertEqual(weather_detail?.daily?[0].dt, 1708664400, "Daily dt should be 1708664400")
        XCTAssertEqual(weather_detail?.daily?[0].pop, 0.63, "Daily pop should be 0.6")
        XCTAssertEqual(weather_detail?.daily?[0].weather?[0].icon, "10d", "Daily weather icon should be 10d")
        XCTAssertEqual(weather_detail?.daily?[0].weather?[0].main, "Rain", "Daily weather main should be Rain")
        XCTAssertEqual(weather_detail?.daily?[0].temp?.min, 23.9, "Daily temp min should be 23.9")
        XCTAssertEqual(weather_detail?.daily?[0].temp?.max, 32.68, "Daily temp max should be 32.68")
        XCTAssertEqual(weather_detail?.daily?[0].temp?.day, 32.06, "Daily temp day should be 32.06")
        
        XCTAssertEqual(weather_detail?.hourly?[0].dt, 1708675200, "Hourly dt should be 1708675200")
        XCTAssertEqual(weather_detail?.hourly?[0].temp, 32.68, "Hourly temp should be 32.68")
        XCTAssertEqual(weather_detail?.hourly?[0].clouds, 40, "Hourly temp should be 40")
        XCTAssertEqual(weather_detail?.hourly?[0].weather?[0].icon, "10d", "Hourly weather icon should be 10d")
        XCTAssertEqual(weather_detail?.hourly?[0].weather?[0].main, "Rain", "Hourly weather main should be Rain")
    }
    
    func test_with_missing_file_error_thrown() {
        XCTAssertThrowsError(try StaticJSONMapper.decode(file: "", type: WeatherModel.self), "An error should be thrown (WeatherModel)")
        
        do {
            _ = try StaticJSONMapper.decode(file: "", type: WeatherModel.self)
            
        }catch {
            guard let mappingError = error as? StaticJSONMapper.MappingError else {
                XCTFail("This is the wrong type of error for missing files (WeatherModel)")
                return
            }
            
            XCTAssertEqual(
                mappingError,
                StaticJSONMapper.MappingError.failedToGetContents,
                "This should be a failed to get contents error (WeatherModel)"
            )
        }
    }
    
    func test_with_invalid_file_error_thrown() {
        XCTAssertThrowsError(try StaticJSONMapper.decode(file: "dfddf", type: WeatherModel.self), "An error should be thrown (WeatherModel)")
        
        do {
            _ = try StaticJSONMapper.decode(file: "dfddf", type: WeatherModel.self)
            
        }catch {
            guard let mappingError = error as? StaticJSONMapper.MappingError else {
                XCTFail("This is the wrong type of error for missing files (WeatherModel)")
                return
            }
            
            XCTAssertEqual(mappingError,
                           StaticJSONMapper.MappingError.failedToGetContents,
                           "This should be a failed to get contents error (WeatherModel)"
            )
        }
    }
}
