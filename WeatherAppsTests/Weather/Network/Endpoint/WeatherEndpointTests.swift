//
//  WeatherEndpointTests.swift
//  WeatherAppsTests
//
//  Created by Tow Ching Shen on 23/02/2024.
//

import XCTest
import CoreLocation
@testable import WeatherApps

final class WeatherEndpointTests: XCTestCase {
    func test_with_detail_endpoint_request_is_valid() {
        let location: CLLocation = CLLocation(latitude: 33.44, longitude: -94.04)
        let endpoint: WeatherEndpoint = .detail(location: location)
        
        XCTAssertEqual(endpoint.host, "api.openweathermap.org", "The host should be api.openweathermap.org")
        XCTAssertEqual(endpoint.path, "/data/2.5/onecall", "The path should be /data/2.5/onecall")
        XCTAssertEqual(endpoint.methodType, .GET, "The methodType should be .GET")
        XCTAssertEqual(
            endpoint.queryItems,
            [
                "lat": "\(location.coordinate.latitude)",
                "lon": "\(location.coordinate.longitude)",
                "units": "metric",
                "exclude": "minutely",
                "appid": endpoint.weather_api_key
            ],
            "The queryItems should be lat:\(location.coordinate.latitude), lon: \(location.coordinate.longitude), units: metric, exclude: minutely, appid: d0c30802f983cb31a08e3f5c34b9a4a1"
        )
        
        XCTAssertEqual(
            endpoint.url?.absoluteString,
            "https://api.openweathermap.org/data/2.5/onecall?appid=d0c30802f983cb31a08e3f5c34b9a4a1&exclude=minutely&lat=33.44&lon=-94.04&units=metric",
            "The generated doesn't match our endpoint (Weather)"
        )
    }
}
