//
//  WeatherNetworkManagerTests.swift
//  WeatherAppsTests
//
//  Created by Tow Ching Shen on 22/02/2024.
//

import XCTest
@testable import WeatherApps

final class WeatherNetworkManagerTests: XCTestCase {
    private var session: URLSession!
    private var url: URL!
    
    override func setUp() {
        url = URL(string: "https://api.openweathermap.org/data")
        
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLSessionProtocol.self]
        session = URLSession(configuration: configuration)
    }
    
    override func tearDown() {
        url = nil
        session = nil
    }

    func test_with_successful_response_response_is_valid() async throws {
        guard let path = Bundle.main.path(forResource: "WeatherDetail", ofType: "json"),
              let data = FileManager.default.contents(atPath: path) else {
            XCTFail("Failed to get the WeatherDetail file")
            return
        }
        
        MockURLSessionProtocol.loadingHandler = {
            let response = HTTPURLResponse(
                url: self.url,
                statusCode: 200,
                httpVersion: nil,
                headerFields: nil
            )
            
            return (response!, data, nil)
        }
        
        let res = try await WeatherNetworkManager.shared.request(
            session: session,
            endpoint: .detail(location: nil),
            type: WeatherModel.self
        )

        let staticJSON = try StaticJSONMapper.decode(file: "WeatherDetail", type: WeatherModel.self)

        XCTAssertEqual(res, staticJSON, "The returned Weather response should be decoded properly")
    }
    
    func test_with_unsucessful_response_code_in_invalid_range_is_invalid() async {
        let invalidStatusCode: Int = 400
        
        MockURLSessionProtocol.loadingHandler = {
            let response = HTTPURLResponse(
                url: self.url,
                statusCode: invalidStatusCode,
                httpVersion: nil,
                headerFields: nil
            )
            
            return (response!, nil, nil)
        }
        
        do {
            _ = try await WeatherNetworkManager.shared.request(
                session: session,
                endpoint: .detail(location: nil),
                type: WeatherModel.self
            )
            
        }catch {
            guard let networkError = error as? NetworkError else {
                XCTFail("Got the wrong type of error, expecting Weather NetworkingManager NetworkingError")
                return
            }
            
            XCTAssertEqual(
                networkError,
                NetworkError.invalidStatusCode(statusCode: invalidStatusCode),
                "Error should be a Weather networking error which throws an invalid status code"
            )
        }
    }
    
    func test_with_successful_response_with_invalid_json_is_invalid() async {
        guard let path = Bundle.main.path(forResource: "WeatherDetail", ofType: "json"),
              let data = FileManager.default.contents(atPath: path) else {
            XCTFail("Failed to get the WeatherDetail file")
            return
        }
        
        MockURLSessionProtocol.loadingHandler = {
            let response = HTTPURLResponse(
                url: self.url,
                statusCode: 200,
                httpVersion: nil,
                headerFields: nil
            )
            
            return (response!, data, nil)
        }
        
        do {
            _ = try await WeatherNetworkManager.shared.request(
                session: session,
                endpoint: .detail(location: nil),
                type: CurrentWeatherDetail.self
            )
            
        }catch {
            if error is NetworkError {
                XCTFail("The error should be a system decoding error (Weather)")
            }
        }
    }
}
