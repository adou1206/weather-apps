//
//  WeatherViewModelSuccessTests.swift
//  WeatherAppsTests
//
//  Created by Tow Ching Shen on 26/02/2024.
//

import XCTest
import CoreLocation
@testable import WeatherApps

final class WeatherViewModelSuccessTests: XCTestCase {
    private var networkMock: WeatherNetworkManagerImpl!
    private var vm: WeatherViewModel!
    
    override func setUp() {
        networkMock = NetworkManagerWeatherResponseSuccessMock()
        vm = WeatherViewModel(networkManagerImpl: networkMock)
    }
    
    override func tearDown() {
        networkMock = nil
        vm = nil
    }

    func test_with_successful_response_weather_details_is_set() async throws {
        XCTAssertFalse(vm.isLoading, "The Weather view model shouldn't be loading any data")
        
        defer {
            XCTAssertFalse(vm.isLoading, "The Weather view model shouldn't be loading any data")
        }
        
        await vm.fetchWeather(for: CLLocation(latitude: 33.44, longitude: -94.04))
        
        XCTAssertNotNil(vm.weatherModel, "The weather detail in the view model should not be nil")
        
        let weatherDetailsData = try StaticJSONMapper.decode(file: "WeatherDetail", type: WeatherModel.self)
        
        XCTAssertEqual(vm.weatherModel, weatherDetailsData, "The Weather response from our netwrk mock should match")
    }
}
