//
//  WeatherViewModelFailureTests.swift
//  WeatherAppsTests
//
//  Created by Tow Ching Shen on 26/02/2024.
//

import XCTest
import CoreLocation
@testable import WeatherApps

final class WeatherViewModelFailureTests: XCTestCase {
    private var networkMock: WeatherNetworkManagerImpl!
    private var vm: WeatherViewModel!

    override func setUp() {
        networkMock = NetworkManagerWeatherResponseFailureMock()
        vm = WeatherViewModel(networkManagerImpl: networkMock)
    }
    
    override func tearDown() {
        networkMock = nil
        vm = nil
    }
    
    func test_with_unsuccessful_response_error_is_handled() async {
        XCTAssertFalse(vm.isLoading, "The Weather view model shouldn't be loading any data")
        
        defer {
            XCTAssertFalse(vm.isLoading, "The Weather view model shouldn't be loading any data")
        }
        
        await vm.fetchWeather(for: CLLocation(latitude: 33.44, longitude: -94.04))
        
        XCTAssertTrue(vm.hasError, "The Weather view model error should be true")
        
        XCTAssertNotNil(vm.error, "The Weather view model error should not be nil")
    }
}
